﻿
using AirBox.Common.DataHelper;
using AirBox.Model;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
    public class UserDataLayer
    {
        public User UserLogin(string EmailPhone, string Password)
        {
            User user = new User();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("ValidateLogin", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@EmailPhone", SqlDbType.VarChar).Value = EmailPhone;
                    sqlCommand.Parameters.Add("@Password", SqlDbType.VarChar).Value = Password;

                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        user = ((User)SqlReaderHelper.GetAs(sqlreader, typeof(User)));
                    }
                }
            }
            return user;
        }

        public List<Product> GetAllAssignedPeoductDetailsByVendorId(int Id)
        {
            List<Product> Productlist = new List<Product>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllAssignedPeoductDetailsByVendorId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        Productlist.Add((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return Productlist;
        }

        public List<Product> GetAssignedProductDetailsByDeliveryBoyId(int Id)
        {
            List<Product> Productlist = new List<Product>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAssignedProductDetailsByDeliveryBoyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        Productlist.Add((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return Productlist;
        }

        public Product GetProductDetailsbyId(int Id)
        {
            Product product = new Product();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetProductDetailsbyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        product = ((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return product;
        }
    }
}
