﻿using AirBox.Common.DataHelper;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
    public class DeliveryBoyDataLayer
    {
        public int AddEditDeliveryBoyDetails(DeliveryBoy deliveryBoy)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("AddEditDeliveryBoyDetails", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = deliveryBoy.Id;
                    sqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = deliveryBoy.FirstName;
                    sqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar).Value = deliveryBoy.LastName;
                    sqlCommand.Parameters.Add("@EmailId", SqlDbType.VarChar).Value = deliveryBoy.EmailId;
                    sqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = deliveryBoy.PhoneNumber;
                    sqlCommand.Parameters.Add("@AlternativePhonenmber", SqlDbType.VarChar).Value = deliveryBoy.AlternativePhonenmber;
                    sqlCommand.Parameters.Add("@DateOfJoining", SqlDbType.DateTime).Value = deliveryBoy.DateOfJoining;
                    sqlCommand.Parameters.Add("@EducationDetails", SqlDbType.VarChar).Value = deliveryBoy.EducationDetails;
                    sqlCommand.Parameters.Add("@VehicleNumber", SqlDbType.VarChar).Value = deliveryBoy.VehicleNumber;
                    sqlCommand.Parameters.Add("@DrivingLicenseNumber", SqlDbType.VarChar).Value = deliveryBoy.DrivingLicenseNumber;
                    sqlCommand.Parameters.Add("@PanNumber", SqlDbType.VarChar).Value = deliveryBoy.PanNumber;
                    sqlCommand.Parameters.Add("@PinCodeId", SqlDbType.Int).Value = deliveryBoy.PinCodeId;
                    sqlCommand.Parameters.Add("@vendorId", SqlDbType.Int).Value = deliveryBoy.VendorId;
                    sqlCommand.Parameters.Add("@Password", SqlDbType.VarChar).Value = deliveryBoy.Password;
                    sqlCommand.Parameters.Add("@GeoAreaMap", SqlDbType.VarChar).Value = deliveryBoy.GeoAreaMap;
                    sqlCommand.Parameters.Add("@RepositoryFile", SqlDbType.VarChar).Value = deliveryBoy.RepositoryFile;

                    sqlConnection.Open();
                    int dbId= sqlCommand.ExecuteNonQuery();
                    return dbId;
                }
            }
        }

        public List<DeliveryBoy> GetAllDeliveryBoyDetailsList()
        {
            List<DeliveryBoy> deliveryBoyDetails = new List<DeliveryBoy>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllDeliveryBoyDetails", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        deliveryBoyDetails.Add((DeliveryBoy)SqlReaderHelper.GetAs(sqlreader, typeof(DeliveryBoy)));
                    }
                }
            }
            return deliveryBoyDetails;
        }

        public DeliveryBoy GetDeliverBoyDetailsbyId(int Id)
        {
            DeliveryBoy deliveryBoyDetails = new DeliveryBoy();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetDeliverBoyDetailsbyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        deliveryBoyDetails=(DeliveryBoy)SqlReaderHelper.GetAs(sqlreader, typeof(DeliveryBoy));
                    }
                }
            }
            return deliveryBoyDetails;
        }
    }
}
