﻿using AirBox.Common.DataHelper;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
   public class VendorDataLayer
    {
        public int AddEditVendorDetails(Vendor vendorDetails)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("AddEditVendorDetails", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = vendorDetails.Id;
                    sqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar).Value =vendorDetails.FirstName;
                    sqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar).Value = vendorDetails.LastName;
                    sqlCommand.Parameters.Add("@Email", SqlDbType.VarChar).Value = vendorDetails.Email;
                    sqlCommand.Parameters.Add("@Password", SqlDbType.VarChar).Value = vendorDetails.Password;
                    sqlCommand.Parameters.Add("@PinId", SqlDbType.Int).Value = vendorDetails.PinId;
                    sqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = vendorDetails.PhoneNumber;
                    sqlCommand.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendorDetails.Address;
                    sqlCommand.Parameters.Add("@AlternativePhoneNumber", SqlDbType.VarChar).Value = vendorDetails.AlternativePhoneNumber;
                    sqlCommand.Parameters.Add("@UserRoleId", SqlDbType.Int).Value = vendorDetails.UserRoleId;


                    sqlConnection.Open();
                    return Convert.ToInt32(sqlCommand.ExecuteScalar());

                }
            }
        }

        public List<Vendor> GetVendorDetailsList()
        {
            List<Vendor> vendotlist = new List<Vendor>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllVedorDetailsList", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        vendotlist.Add((Vendor)SqlReaderHelper.GetAs(sqlreader, typeof(Vendor)));
                    }
                }
            }
            return vendotlist;
        }

        public Vendor GetVendorDetailsbyId(int Id)
        {
            Vendor vendotDetails = new Vendor();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetVendorDetailsbyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        vendotDetails=((Vendor)SqlReaderHelper.GetAs(sqlreader, typeof(Vendor)));
                    }
                }
            }
            return vendotDetails;
        }

        public List<UserMaritalStatus> GetUserMaritalStatusList()
        {
            List<UserMaritalStatus> vendotlist = new List<UserMaritalStatus>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetUserMaritalStatusList", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        vendotlist.Add((UserMaritalStatus)SqlReaderHelper.GetAs(sqlreader, typeof(UserMaritalStatus)));
                    }
                }
            }
            return vendotlist;
        }

        public Vendor GetFranchiseProfileDetailsbyId(int UserId)
        {
            Vendor vendotDetails = new Vendor();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetFranchiseProfileDetailsbyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = UserId;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        vendotDetails = ((Vendor)SqlReaderHelper.GetAs(sqlreader, typeof(Vendor)));
                    }
                }
            }
            return vendotDetails;
        }

        public List<UserGender> GetUserGenderList()
        {
            List<UserGender> genderlistlist = new List<UserGender>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetUserGenderList", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        genderlistlist.Add((UserGender)SqlReaderHelper.GetAs(sqlreader, typeof(UserGender)));
                    }
                }
            }
            return genderlistlist;
        }
    }
}
