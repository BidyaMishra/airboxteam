﻿using AirBox.Common.DataHelper;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
   public class PinDataLayer
    {
       public List<MasterDistrict> GetAlDistrictByDistrictName()
       {
           List<MasterDistrict> lstMasterDistrict = new List<MasterDistrict>();
           using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
           {
               using (SqlCommand sqlCommand = new SqlCommand("GetAlDistrictByDistrictName", sqlConnection))
               {
                   sqlCommand.CommandType = CommandType.StoredProcedure;
                   //sqlCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = districtName;
                   sqlConnection.Open();
                   SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                   while (sqlreader.Read())
                   {
                       lstMasterDistrict.Add((MasterDistrict)SqlReaderHelper.GetAs(sqlreader, typeof(MasterDistrict)));
                   }
               }
           }
           return lstMasterDistrict;
       }

       public List<MasterArea> GetAllAreaByAreaNameAndDistrictId(int districtId)
       {
           List<MasterArea> lstMasterArea = new List<MasterArea>();
           using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
           {
               using (SqlCommand sqlCommand = new SqlCommand("GetAllAreaByAreaNameAndDistrictId", sqlConnection))
               {
                   sqlCommand.CommandType = CommandType.StoredProcedure;
                   //sqlCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = areaName;
                   sqlCommand.Parameters.Add("@DistrcitId", SqlDbType.Int).Value = districtId;
                   sqlConnection.Open();
                   SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                   while (sqlreader.Read())
                   {
                       lstMasterArea.Add((MasterArea)SqlReaderHelper.GetAs(sqlreader, typeof(MasterArea)));
                   }
               }
           }
           return lstMasterArea;
       }

       public List<MasterLocality> GetAllLocalAreaByLocalAreaNameAndAreaId(int areaId)
       {
           List<MasterLocality> lstMasterLocality = new List<MasterLocality>();
           using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
           {
               using (SqlCommand sqlCommand = new SqlCommand("GetAllLocalAreaByLocalAreaNameAndAreaId", sqlConnection))
               {
                   sqlCommand.CommandType = CommandType.StoredProcedure;
                   //sqlCommand.Parameters.Add("@LocaAreaName", SqlDbType.NVarChar).Value = localAreaName;
                   sqlCommand.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaId;
                   sqlConnection.Open();
                   SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                   while (sqlreader.Read())
                   {
                       lstMasterLocality.Add((MasterLocality)SqlReaderHelper.GetAs(sqlreader, typeof(MasterLocality)));
                   }
               }
           }
           return lstMasterLocality;
       }



       public MasterPin GetAllPinByLocalAreaId(int localAreaId)
       {
           MasterPin masterPin = new MasterPin();
           using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
           {
               using (SqlCommand sqlCommand = new SqlCommand("GetAllPinByLocalAreaId", sqlConnection))
               {
                   sqlCommand.CommandType = CommandType.StoredProcedure;
                   sqlCommand.Parameters.Add("@LocalAreaId", SqlDbType.Int).Value = localAreaId;
                   sqlConnection.Open();
                   SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                   while (sqlreader.Read())
                   {
                       masterPin = (MasterPin)SqlReaderHelper.GetAs(sqlreader, typeof(MasterPin));
                   }
               }
           }
           return masterPin;
       }
    }
}
