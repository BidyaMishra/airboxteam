﻿
using AirBox.Common.DataHelper;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
  public  class AdminDataLayer
    {
        

        public List<Product> GetProductDetailsByAdmin()
        {
            List<Product> Productlist = new List<Product>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetProductDetailsByAdmin", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        Productlist.Add((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return Productlist;
        }

        public List<Vendor> GetVendorList()
        {
            List<Vendor> vendorlist = new List<Vendor>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetVendorList", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        vendorlist.Add((Vendor)SqlReaderHelper.GetAs(sqlreader, typeof(Vendor)));
                    }
                }
            }
            return vendorlist;
        }

        public List<Company> GetCompanyDetails()
        {
            List<Company> companyDetails = new List<Company>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllCompanyDetails", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        companyDetails.Add((Company)SqlReaderHelper.GetAs(sqlreader, typeof(Company)));
                    }
                }
            }
            return companyDetails;
        }

        public int DeleteCompanyDetailsbyId(int Id)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("DeleteCompanyDetailsbyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.VarChar).Value =Id;
                  
                    sqlConnection.Open();
                    var returnValue = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    return returnValue;
                }
            }
        }

        public void AddEditCompanyDetails(Company CompanyDetails)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("AddEditCompanyDetails", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = CompanyDetails.Id;
                    sqlCommand.Parameters.Add("@ShippingCompanyName", SqlDbType.VarChar).Value = CompanyDetails.ShippingCompanyName;
                    sqlCommand.Parameters.Add("@Email", SqlDbType.VarChar).Value = CompanyDetails.Email;
                    sqlCommand.Parameters.Add("@HeadQuaterAddress", SqlDbType.VarChar).Value = CompanyDetails.HeadQuaterAddress;
                    sqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = CompanyDetails.PhoneNumber;
                    sqlCommand.Parameters.Add("@LocalAddress", SqlDbType.VarChar).Value = CompanyDetails.LocalAddress;
                
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                   
                }
            }
        }

        public Company GetCompanyDetailsbyId(int Id)
        {
            Company companyDetails = new Company();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllCompanyDetailsByCompanyId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        companyDetails=(Company)SqlReaderHelper.GetAs(sqlreader, typeof(Company));
                    }
                }
            }
            return companyDetails;
        }


        public int AddEditProductByAdmin(Product product)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("AddEditProductByAdmin", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = product.Id;
                    sqlCommand.Parameters.Add("@ProductName", SqlDbType.VarChar).Value = product.ProductDetails;
                    sqlCommand.Parameters.Add("@CustomerAddress", SqlDbType.VarChar).Value = product.CustomerAddress;
                    sqlCommand.Parameters.Add("@CustomerPin", SqlDbType.VarChar).Value = product.CustomerPin;
                    sqlCommand.Parameters.Add("@CustomerPhoneNumber", SqlDbType.VarChar).Value = product.CustALtPhnNumber;
                    sqlCommand.Parameters.Add("@CustALtPhnNumber", SqlDbType.VarChar).Value = product.CustALtPhnNumber;
                    sqlCommand.Parameters.Add("@CustomerEmail", SqlDbType.VarChar).Value = product.CustomerEmail;
                    sqlCommand.Parameters.Add("@PaymentStatus", SqlDbType.VarChar).Value = product.PayemntStautus;
                    sqlCommand.Parameters.Add("@ShippingCompanyName", SqlDbType.VarChar).Value = product.ShippingCompanyName;
                    sqlCommand.Parameters.Add("@ExpetedDeliveryDate", SqlDbType.DateTime).Value = product.ExpectedDeliveryDate;
                    sqlCommand.Parameters.Add("@ShippingDate", SqlDbType.DateTime).Value = product.ExpectedDeliveryDate;
                    //sqlCommand.Parameters.Add("@ProductStatusId", SqlDbType.Int).Value = product.ProductStatus;

                    sqlConnection.Open();
                    return Convert.ToInt32(sqlCommand.ExecuteScalar());

                }
            }
        }


        public Product GetProductDetailsByProductId(int Id)
        {
            Product product = new Product();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetProductDetailsByProductId", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        product = (Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product));
                    }
                }
            }
            return product;
        }


        public List<ProductStatusMaster> GetAllProductStatus()
        {
            List<ProductStatusMaster> lstProductStatusMaster = new List<ProductStatusMaster>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllProductStatus", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        lstProductStatusMaster.Add((ProductStatusMaster)SqlReaderHelper.GetAs(sqlreader, typeof(ProductStatusMaster)));
                    }
                }
            }
            return lstProductStatusMaster;
        }


        public List<ShippingCompanyMaster> GetAllShippingComapny()
        {
            List<ShippingCompanyMaster> lstShippingCompanyMaster = new List<ShippingCompanyMaster>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllShippingComapny", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        lstShippingCompanyMaster.Add((ShippingCompanyMaster)SqlReaderHelper.GetAs(sqlreader, typeof(ShippingCompanyMaster)));
                    }
                }
            }
            return lstShippingCompanyMaster;
        }


        public List<PaymentStatusMaster> GetAllPaymentStatusMaster()
        {
            List<PaymentStatusMaster> lstPaymentStatusMaster = new List<PaymentStatusMaster>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllPaymentStatusMaster", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        lstPaymentStatusMaster.Add((PaymentStatusMaster)SqlReaderHelper.GetAs(sqlreader, typeof(PaymentStatusMaster)));
                    }
                }
            }
            return lstPaymentStatusMaster;
        }


        public List<Product> GetFranchiseAssignProduct(int UserId,int UserRoleId)
        {

            List<Product> lstproductdetails = new List<Product>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAssignedProduct", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                    sqlCommand.Parameters.Add("@UserRoleId", SqlDbType.Int).Value = UserRoleId;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        lstproductdetails.Add((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return lstproductdetails;
        }

        public List<Product> GetAllUnAssignProductToAdmin()
        {
            List<Product> lstproductdetails = new List<Product>();
            using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("GetAllUnAssignedProduct", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();
                    SqlDataReader sqlreader = sqlCommand.ExecuteReader();
                    while (sqlreader.Read())
                    {
                        lstproductdetails.Add((Product)SqlReaderHelper.GetAs(sqlreader, typeof(Product)));
                    }
                }
            }
            return lstproductdetails;
        }

        public int AssignProduct(int productId,int pinId, string pinVal)
        {
            using (SqlConnection sqlConnection = new SqlConnection((SqlReaderHelper.DbConnectionString)))
            {
                using (SqlCommand sqlCommand = new SqlCommand("AssignProduct", sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value =productId;
                    sqlCommand.Parameters.Add("@PinId", SqlDbType.Int).Value = pinId;
                    sqlCommand.Parameters.Add("@PinVal", SqlDbType.VarChar).Value = pinVal;

                    sqlConnection.Open();
                    return Convert.ToInt32(sqlCommand.ExecuteScalar());

                }
            }
        }
    }
}
