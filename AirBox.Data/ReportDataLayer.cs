﻿using AirBox.Common.DataHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Data
{
    public class ReportDataLayer
    {

        public List<string> AddEditProduct(DataTable productDetails)
        {
            List<string> accounts = new List<string>();
            foreach (DataRow row in productDetails.Rows)
            {

                using (SqlConnection sqlConnection = new SqlConnection(SqlReaderHelper.DbConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("AddEditProduct", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        if (!CheckIfStringNull(row["Product Name"]))
                            sqlCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = row["Product Name"];

                        if (!CheckIfStringNull(row["Customer Address"]))
                            sqlCommand.Parameters.Add("@CustomerAddress", SqlDbType.NVarChar).Value = row["Customer Address"];

                        if (!CheckIfStringNull(row["Customer Pin"]))
                            sqlCommand.Parameters.Add("@CustomerPin", SqlDbType.NVarChar).Value = row["Customer Pin"];

                        if (!CheckIfStringNull(row["Customer Phone Number"]))
                            sqlCommand.Parameters.Add("@CustomerPhoneNumber", SqlDbType.NVarChar).Value = row["Customer Phone Number"];

                        if (!CheckIfStringNull(row["Cust ALt PhnNumber"]))
                            sqlCommand.Parameters.Add("@CustALtPhnNumber", SqlDbType.NVarChar).Value = row["Cust ALt PhnNumber"];

                        if (!CheckIfStringNull(row["Customer Email"]))
                            sqlCommand.Parameters.Add("@CustomerEmail", SqlDbType.NVarChar).Value = row["Customer Email"];

                        if (!CheckIfStringNull(row["Payment Staus"]))
                            sqlCommand.Parameters.Add("@PaymentStatus", SqlDbType.NVarChar).Value = row["Payment Staus"];

                        if (!CheckIfStringNull(row["Shipping Company"]))
                            sqlCommand.Parameters.Add("@ShippingCompanyName", SqlDbType.Int).Value = row["Shipping Company"];

                        if (!CheckIfStringNull(row["Expected Delivery Date"]))
                            sqlCommand.Parameters.Add("@ExpetedDeliveryDate", SqlDbType.DateTime).Value = row["Expected Delivery Date"];

                        if (!CheckIfStringNull(row["Shipping Date"]))
                            sqlCommand.Parameters.Add("@ShippingDate", SqlDbType.DateTime).Value = row["Shipping Date"];

                        if (!CheckIfStringNull(row["Product Status"]))
                            sqlCommand.Parameters.Add("@ProductStatusId", SqlDbType.Int).Value = row["Product Status"];

                       
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();


                    }
                }

            }

            return accounts;
        }

        private bool CheckIfStringNull(object input)
        {
            if (string.IsNullOrEmpty(Convert.ToString(input)) || Convert.ToString(input).ToLower() == "null")
            {
                return true;
            }
            return false;
        }
    }
}
