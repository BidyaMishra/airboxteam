﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
   public class UserMaritalStatus
    {
        public int MaritalStatusId { get; set; }
        public string MaritalStatus { get; set; }
    }
}
