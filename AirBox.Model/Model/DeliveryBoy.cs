﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
   public class DeliveryBoy
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? AreaId { get; set; }

        public string EmailId { get; set; }

        public string PhoneNumber { get; set; }

        public string AlternativePhonenmber { get; set; }

        public DateTime? DateOfJoining { get; set; }

        public string EducationDetails { get; set; }

        public string VehicleNumber { get; set; }

        public string DrivingLicenseNumber { get; set; }

        public string PanNumber { get; set; }

        public int? PinCodeId { get; set; }

        public int? VendorId { get; set; }

        public string Password { get; set; }

        public bool? IsOnLeave { get; set; }

        public string GeoAreaMap { get; set; }

        public string RepositoryFile { get; set; }

        public int? UserRelationId { get; set; }
        public string Name { get; set; }
    }
}
