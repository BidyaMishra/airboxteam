﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
  
       public class ProductStatusMaster
       {
           public int Id { get; set; }

           public string ProductStatus { get; set; }

       }

}
