﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class ShippingCompanyMaster
    {
        public int Id { get; set; }

        public string ShippingCompanyName { get; set; }

        public string PhoneNumber { get; set; }

        public string HeadQuaterAddress { get; set; }

        public string Email { get; set; }

        public string LocalAddress { get; set; }

        public bool? IsDeleted { get; set; }

    }

}
