﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class Vendor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternativePhoneNumber { get; set; }
        public string Address { get; set; }
        public int PinId { get; set; }
        public int UserRoleId { get; set; }
        public string Password { get; set; }
        public int GenderId { get; set; }
        public string City { get; set; }
        public DateTime DOB { get; set; }
        public int MaritalStatusId { get; set; }

        public string MaritalStatus { get; set; }

        public string Gender { get; set; }

        public string FaceBook { get; set; }
        public int UserRelationId { get; set; }
             
    }
}
