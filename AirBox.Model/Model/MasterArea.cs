﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class MasterArea
    {
        public int Id { get; set; }

        public int? MasterDistrictId { get; set; }

        public string Area { get; set; }

    }

}
