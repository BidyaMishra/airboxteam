﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public string ProductDetails { get; set; }
        public string CustomerAddress { get; set; }
        public string CustALtPhnNumber { get; set; }
        public string CustomerPin { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string ProductStatus { get; set; }
        public string PayemntStautus { get; set; }
        public string ShippingCompanyName { get; set; }
        public int VendorId { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}")]
        public DateTime? ExpectedDeliveryDate { get; set; }
        public int ShippingCompanyId { get; set; }
        public int PSMID { get; set; }
        public int PMID { get; set; }
        public decimal Cost { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}")]
        public DateTime ShippingDate { get; set; }
        public decimal Weight { get; set; }
        public string DeliveryBoyName { get; set; }
        public string FranchiesName { get; set; }
        public string DeliveryBoyMobileNo { get; set; }


    }
}
