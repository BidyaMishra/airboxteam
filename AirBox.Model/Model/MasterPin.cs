﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class MasterPin
    {
        public int Id { get; set; }

        public int? MasterLocalAreaId { get; set; }

        public string Pin { get; set; }

    }
}
