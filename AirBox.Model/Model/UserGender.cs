﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
   public class UserGender
    {
        public int GenderId { get; set; }

        public string Gender { get; set; }
    }
}
