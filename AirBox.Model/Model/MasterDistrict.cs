﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class MasterDistrict
    {
        public int Id { get; set; }

        public string District { get; set; }

    }

}
