﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.Model
{
    public class MasterLocality
    {
        public int Id { get; set; }

        public int? MasterAreaId { get; set; }

        public string Locality { get; set; }

    }

}
