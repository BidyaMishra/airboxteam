﻿using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Model.ViewModel
{
    public class ProductViewModel
    {
        public List<Product> ProductDetails;
        public Product ProductItem;
    }
}
