﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AirBox.Common.DataHelper
{
    public static class SqlReaderHelper
    {
        public static readonly string DbConnectionString = ConfigurationManager.AppSettings["conString"];

        private static bool IsNullableType(Type theValueType)
        {
            return (theValueType.IsGenericType && theValueType.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Returns the value, of type T, from the SqlDataReader, accounting for both generic and non-generic types.
        /// </summary>
        /// <typeparam name="T">T, type applied</typeparam>
        /// <param name="theReader">The SqlDataReader object that queried the database</param>
        /// <param name="theColumnName">The column of data to retrieve a value from</param>
        /// <returns>T, type applied; default value of type if database value is null</returns>
        public static T GetValue<T>(this SqlDataReader theReader, string theColumnName)
        {
            // Read the value out of the reader by string (column name); returns object
            object theValue = theReader[theColumnName];

            // Cast to the generic type applied to this method (i.e. int?)
            Type theValueType = typeof(T);

            // Check for null value from the database
            if (DBNull.Value != theValue)
            {
                // We have a null, do we have a nullable type for T?
                if (!IsNullableType(theValueType))
                {
                    // No, this is not a nullable type so just change the value's type from object to T
                    return (T)Convert.ChangeType(theValue, theValueType);
                }
                // Yes, this is a nullable type so change the value's type from object to the underlying type of T
                NullableConverter theNullableConverter = new NullableConverter(theValueType);

                return (T)Convert.ChangeType(theValue, theNullableConverter.UnderlyingType);
            }

            // The value was null in the database, so return the default value for T; this will vary based on what T is (i.e. int has a default of 0)
            return default(T);
        }

        public static Object GetAs(SqlDataReader reader, Type objectToReturnType)
        {
            // Create a new Object
            Object newObjectToReturn = Activator.CreateInstance(objectToReturnType);
            // Get all the properties in our Object
            PropertyInfo[] props = objectToReturnType.GetProperties();
            // For each property get the data from the reader to the object

            var columnNames = reader.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"] as string).ToList();

            for (int i = 0; i < props.Length; i++)
            {
                try
                {
                    // if Object name is present in sqlReader continue the loop for next 
                    if (!columnNames.Contains(props[i].Name, StringComparer.OrdinalIgnoreCase))
                    {
                        continue;
                    }
                    if (reader[props[i].Name] == DBNull.Value)
                    {
                        continue;
                    }

                    props[i].SetValue(newObjectToReturn, reader[props[i].Name], null);
                    objectToReturnType.InvokeMember(props[i].Name, BindingFlags.SetProperty, null, newObjectToReturn, new[] { reader[props[i].Name] });
                }
                catch
                {
                    throw;
                }
            }
            return newObjectToReturn;
        }


        public static bool Contains(this string target, string value, StringComparison comparison)
        {
            return target.IndexOf(value, comparison) >= 0;
        }
    }
}

