﻿using AirBox.Data;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
    public class DeliveryBoyBusinessLayer
    {
        DeliveryBoyDataLayer _objDeliveryBoyDataLayer = new DeliveryBoyDataLayer();
        public int AddEditDeliveryBoyDetails(DeliveryBoy deliveryBoy)
        {
            return _objDeliveryBoyDataLayer.AddEditDeliveryBoyDetails(deliveryBoy);
        }

        public List<DeliveryBoy> GetAllDeliveryBoyDetailsList()
        {
            return _objDeliveryBoyDataLayer.GetAllDeliveryBoyDetailsList();
        }

        public DeliveryBoy GetDeliverBoyDetailsbyId(int Id)
        {
            return _objDeliveryBoyDataLayer.GetDeliverBoyDetailsbyId(Id);
        }
    }
}
