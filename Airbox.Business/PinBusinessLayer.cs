﻿using AirBox.Data;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
   public class PinBusinessLayer
    {
       PinDataLayer _objPinDataLayer = new PinDataLayer();

       public List<MasterDistrict> GetAlDistrictByDistrictName()
       {
           return _objPinDataLayer.GetAlDistrictByDistrictName();
       }

       public List<MasterArea> GetAllAreaByAreaNameAndDistrictId(int districtId)
       {
           return _objPinDataLayer.GetAllAreaByAreaNameAndDistrictId(districtId);
       }

       public List<MasterLocality> GetAllLocalAreaByLocalAreaNameAndAreaId(int areaId)
       {
           return _objPinDataLayer.GetAllLocalAreaByLocalAreaNameAndAreaId(areaId);
       }

       public MasterPin GetAllPinByLocalAreaId(int localAreaId)
       {
           return _objPinDataLayer.GetAllPinByLocalAreaId(localAreaId);
       }

    }
}
