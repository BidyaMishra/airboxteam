﻿
using AirBox.Data;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
   public class AdminBusinessLayer
    {
       readonly AdminDataLayer _objAdminDataLayer = new AdminDataLayer();


      

       public List<Product> GetProductDetailsByAdmin()
       {
           return _objAdminDataLayer.GetProductDetailsByAdmin();
       }

       public List<Vendor> GetVendorList()
       {
           return _objAdminDataLayer.GetVendorList();
       }

       public List<Company> GetCompanyDetails()
       {
           return _objAdminDataLayer.GetCompanyDetails();
       }

       public int DeleteCompanyDetailsbyId(int Id)
       {
           return _objAdminDataLayer.DeleteCompanyDetailsbyId(Id);
       }

       public void AddEditCompanyDetails(Company CompanyDetails)
       {
           _objAdminDataLayer.AddEditCompanyDetails(CompanyDetails);
       }

       public Company GetCompanyDetailsbyId(int Id)
       {
         return  _objAdminDataLayer.GetCompanyDetailsbyId(Id);
       }


       public int AddEditProductByAdmin(Product product)
       {

           return _objAdminDataLayer.AddEditProductByAdmin(product);
       }


       public Product GetProductDetailsByProductId(int Id)
       {
           return _objAdminDataLayer.GetProductDetailsByProductId(Id);
       }


       public List<ProductStatusMaster> GetAllProductStatus()
       {
           return _objAdminDataLayer.GetAllProductStatus();
       }

       public List<ShippingCompanyMaster> GetAllShippingComapny()
       {
           return _objAdminDataLayer.GetAllShippingComapny();
       }

       public List<PaymentStatusMaster> GetAllPaymentStatusMaster()
       {
           return _objAdminDataLayer.GetAllPaymentStatusMaster();
       }

       public List<Product> GetFranchiseAssignProduct(int UserId, int UserRoleId)
       {
           return _objAdminDataLayer.GetFranchiseAssignProduct(UserId, UserRoleId);
       }

       public List<Product> GetAllUnAssignProductToAdmin()
       {
           return _objAdminDataLayer.GetAllUnAssignProductToAdmin();
       }
       public int AssignProduct(int productId, int pinId, string pinVal)
       {
          return _objAdminDataLayer.AssignProduct(productId, pinId, pinVal);
       }
    }
}
