﻿using AirBox.Data;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
   public class VendorBusinessLayer
    {
       readonly VendorDataLayer _objVendorDataLayer = new VendorDataLayer();
        public int AddEditVendorDetails(Vendor vendorDetails)
        {
          return  _objVendorDataLayer.AddEditVendorDetails(vendorDetails);
        }

        public List<Vendor> GetVendorDetailsList()
        {
            return _objVendorDataLayer.GetVendorDetailsList();
        }

        public Vendor GetVendorDetailsbyId(int Id)
        {
            return _objVendorDataLayer.GetVendorDetailsbyId(Id);
        }

        public List<UserMaritalStatus> GetUserMaritalStatusList()
        {
            return _objVendorDataLayer.GetUserMaritalStatusList();
        }

        public Vendor GetFranchiseProfileDetailsbyId(int UserId)
        {
            return _objVendorDataLayer.GetFranchiseProfileDetailsbyId(UserId);
        }

        public List<UserGender> GetUserGenderList()
        {
            return _objVendorDataLayer.GetUserGenderList();
        }
    }
}
