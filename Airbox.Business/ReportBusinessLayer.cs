﻿using AirBox.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
    public class ReportBusinessLayer
    {
        ReportDataLayer reportDataLayer = new ReportDataLayer();
        public List<string> AddEditProduct(DataTable productDetails)
        {
            return reportDataLayer.AddEditProduct(productDetails);
        }

    }
}
