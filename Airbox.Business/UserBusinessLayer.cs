﻿
using AirBox.Data;
using AirBox.Model;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbox.Business
{
   public class UserBusinessLayer
    {
       readonly UserDataLayer _objUserDataLayer = new UserDataLayer();
        public User UserLogin(string EmailPhone, string Password)
        {
            return _objUserDataLayer.UserLogin(EmailPhone, Password);
        }

        public List<Product> GetAllAssignedPeoductDetailsByVendorId(int Id)
        {
            return _objUserDataLayer.GetAllAssignedPeoductDetailsByVendorId(Id);
        }

        public List<Product> GetAssignedProductDetailsByDeliveryBoyId(int Id)
        {
            return _objUserDataLayer.GetAssignedProductDetailsByDeliveryBoyId(Id);
        }



        public Product GetProductDetailsbyId(int Id)
        {
            return _objUserDataLayer.GetProductDetailsbyId(Id);
        }
    }
}
