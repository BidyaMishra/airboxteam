﻿using AirBox.Model;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Airbox.Business
{
   public class AirBoxSession
    {
        private const string user = "User";

        public static User User
        {
            get
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[user] == null)
                    return null;
                return (User)HttpContext.Current.Session[user];
            }

            set
            {
                HttpContext.Current.Session[user] = value;
            }
        }
    }
}
