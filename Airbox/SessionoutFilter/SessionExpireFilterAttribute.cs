﻿using Airbox.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Airbox.SessionoutFilter
{

    [AttributeUsage(AttributeTargets.Class |
   AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (AirBoxSession.User != null && AirBoxSession.User.UserRoleId != 0)
                base.OnActionExecuting(filterContext);
            else
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, return result as a simple string, 
                    // and inform calling JavaScript code that a user should be redirected.
                    JsonResult result = new JsonResult { Data = "SessionTimeout" }; //Json("SessionTimeout", "text/html");
                    filterContext.Result = result;
                    filterContext.HttpContext.Response.StatusCode = 401;
                }
                else
                {
                    // For round-trip requests,
                    filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {
                { "Controller", "Login" },
                { "Action", "Index" }
                });
                }
            }
        }
    }
}