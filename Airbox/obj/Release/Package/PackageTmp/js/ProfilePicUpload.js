﻿
//prevent the default handling by cancelling the event
document.ondragover = document.ondragenter = document.ondrop = function (e) {
    e.preventDefault();
    return false;
}

var uploader;
function CuteWebUI_AjaxUploader_OnInitialize() {
    uploader = this;//get uploader object
    uploader.internalobject.SetDialogAccept("image/*");
}

//Fires after all uploads are complete and submit the form
function CuteWebUI_AjaxUploader_OnPostback() {
    var files = uploader.getitems();
    for (var i = 0; i < files.length; i++) {
        var task = files[i];
        //Get the information from server
        var virpath = task.ServerData;

        //Set curtrent image
        $('#imgProfilePic').attr('src', virpath);

        $('#modalImageUploader').modal('hide');
    }

    ResetUpload();

    ShowAlert('User Profile Pic Updated', 'Success ! ', 'success');

    setTimeout(function () {
        //Relode page.
        var url = window.location.href;
        window.location.href = url;
    }, 100)


    //return false to cancel the default form submission
    return false;
}

function StartUpload() {
    uploader.startupload();//Start the upload of all queued files
}
function ResetUpload() {
    //Clear file queue of uploader in the client side
    uploader.reset();
    $("#btnChooseFile").show();
    $('#modalCameraUpload').modal("hide")
}

//Fires when new information about the upload progress for a specific file is available.
function CuteWebUI_AjaxUploader_OnProgress(isuploading, filename, startTime, sentLen, totalLen) {
    if (isuploading) {
        $("#submitbutton").html(Math.floor(sentLen * 100 / totalLen) + "%");
    }
    return false;//hide the default progress bar
}

//Fires when files are selected successfully.
function CuteWebUI_AjaxUploader_OnSelect(files) {
    var task = files[0];
    //Retrieve a list of file items defined by HTML5 <input type=file/>
    var srcfile = task.GetDomFile();
    if (!srcfile || srcfile.type.indexOf("image/") != 0)
        return;

    //if the browse don't support
    if (!window.Uint8Array || !window.ArrayBuffer)
        return;

    $('#modalCameraUpload').modal({})
    $("#submitbutton").html("Upload");


    var div = document.getElementById("imagecropper");
    div.style.display = "block";
    div.innerHTML = "";


    var option = {};
    //specify a file object for <input type=file/>
    option.file = srcfile;
    //specify an element for UI container 
    option.container = div;
    //specify the container padding
    option.padding = 5;
    //When square is set to false, uploader will use rectangular crop-area.
    option.square = false;
    //set the minimum width of an element
    option.minWidth = 64;
    //set the minimum height of an element
    option.minHeight = 64;

    //Fires after a file gets processed
    option.onchange = function (newfile, dataurl, width, height) {

        //use this function to overwrite the uploader file
        task.OverrideDomFile(newfile);

        document.title = width + "x" + height + "," + newfile.size + " bytes";
    }
    uploader.cropper(option);


}

function SwitchDialogControls(mode) {
    //Switch the UI
    var list1 = ["cameravideo", "btnCameraCapture", "btnCameraCancel"];
    var list2 = ["cameracanvas", "btnCameraCommit", "btnCameraReset"];
    if (mode != "video") {
        var t = list1;
        list1 = list2;
        list2 = t;
    }
    for (var i = 0; i < list1.length; i++)
        $("#" + list1[i]).show();
    for (var i = 0; i < list2.length; i++)
        $("#" + list2[i]).hide();
}

var camerainit = false;

function ShowCamera() {

    SwitchDialogControls("video");

    $('#cameraDialog').modal({});

    if (camerainit)
        return;

    var video = document.getElementById("cameravideo");

    function startVideo(url) {
        video.src = url;
        video.play();
    }

    function onerror(error) {
        alert("Camera Error : " + error);//Make sure the camera is not used by another application.
        $('#cameraDialog').modal("hide");
    };

    //access a specific camera device using HTML5

    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function (streamurl) {
            startVideo(streamurl)
        }, onerror);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function (stream) {
            startVideo(window.URL.createObjectURL(stream))
        }, onerror);
    } else if (navigator.mozGetUserMedia) {
        navigator.mozGetUserMedia({ video: true }, function (stream) {
            startVideo(window.URL.createObjectURL(stream))
        }, onerror);
    }
    else {
        alert("Not Support Camera " + navigator.mozGetUserMedia);
        $('#cameraDialog').modal("hide");
    }

    camerainit = true;

}

function CameraCapture() {
    var video = document.getElementById("cameravideo");
    var canvas = document.getElementById("cameracanvas");
    canvas.getContext("2d").drawImage(video, 0, 0, 640, 480);
    SwitchDialogControls("canvas");
}
function CameraCancel() {
    $('#cameraDialog').modal("hide");
}
function CameraCommit() {
    var canvas = document.getElementById("cameracanvas");

    //create an HTML5 canvas file object
    var dataurl = canvas.toDataURL("image/png");
    var bs = atob(dataurl.split(',')[1]);
    var ab = new ArrayBuffer(bs.length);
    var ia = new Uint8Array(ab);
    for (i = 0; i < bs.length; i += 1) {
        ia[i] = bs.charCodeAt(i);
    }
    var file = new Blob([ab], { type: "image/png" });
    file.name = "camera.png";

    //use AddHtml5Files to add file object into uploader queue
    uploader.internalobject.AddHtml5Files([file]);
    $('#cameraDialog').modal("hide");
}
function CameraReset() {
    SwitchDialogControls("video");
}