﻿

$(document).ready(function () {
    GetAlDistrictByDistrictName();
    GetVendorDetailsList();
    $("#ddlDistrict").change(function () {
        GetAllAreaByAreaNameAndDistrictId();
    });
    $("#ddlArea").change(function () {
        GetAllLocalAreaByLocalAreaNameAndAreaId();
    });
    $("#ddlLocaity").change(function () {
        GetAllPinByLocalAreaId();
    });
    $(".pin-border").hide();
});

function GetAlDistrictByDistrictName() {
    
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/DeliveryBoyDashBoard/GetAlDistrictByDistrictName/',
        data: JSON.stringify({ }),
        success: function (res) {
            if (res.length > 0) {
                for (var i in res) {
                    var priceval = res[i];
                    var optionBody = "<option value='" + res[i].Id + "'>" + res[i].District + "</option>";
                    $("#ddlDistrict").append(optionBody);
                }
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}
function GetAllAreaByAreaNameAndDistrictId() {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/DeliveryBoyDashBoard/GetAllAreaByAreaNameAndDistrictId/',
        data: JSON.stringify({ 'districtId': $("#ddlDistrict").val() }),
        success: function (res) {
            if (res.length > 0) {
                for (var i in res) {
                    var priceval = res[i];
                    var optionBody = "<option value='" + res[i].Id + "'>" + res[i].Area + "</option>";
                    $("#ddlArea").append(optionBody);
                }
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}
function GetAllLocalAreaByLocalAreaNameAndAreaId() {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/DeliveryBoyDashBoard/GetAllLocalAreaByLocalAreaNameAndAreaId/',
        data: JSON.stringify({ 'areaId': $("#ddlArea").val() }),
        success: function (res) {
            if (res.length > 0) {
                for (var i in res) {
                    var priceval = res[i];
                    var optionBody = "<option value='" + res[i].Id + "'>" + res[i].Locality + "</option>";
                    $("#ddlLocaity").append(optionBody);
                }
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function GetAllPinByLocalAreaId() {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/DeliveryBoyDashBoard/GetAllPinByLocalAreaId/',
        data: JSON.stringify({ 'localAreaId': $("#ddlLocaity").val() }),
        success: function (res) {
            if (res.Id> 0) {
                $(".pin-border").show();
                    var checkedBoxBody = "<div class='col-sm-2'><div class='form-group fg-line'><input type='checkbox' id='cbPinVal'value='" + res.Id + "' /><label class='p-l-7' id='lblPinVal'>" + res.Pin + "</label></div></div>";
                    $(".pin-border").append(checkedBoxBody);
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function GetVendorDetailsList() {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: '/DeliveryBoyDashBoard/GetVendorDetailsList/',
        data: JSON.stringify({}),
        success: function (res) {
            if (res.length > 0) {
                for (var i in res) {
                    var priceval = res[i];
                    var checkedBoxBody = "<option value='" + res[i].Id + "'>" + res[i].Name + "</option>";
                    $("#ddlVendor").append(checkedBoxBody);
                }
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

