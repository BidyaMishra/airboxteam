﻿var UserProductDetailsModule = angular.module("UserProductDetailsApp", [])

UserProductDetailsModule.controller('UserProductDetailsCntrl', function ($scope, $http, $sce, $filter, $timeout) {
    $scope.ProductDetails = {
        items: [{
            Id: '',
            ProductName: '',
            CustomerAddress: '',
            CustomerPin: '',
            CustomerPhoneNumber: '',
            CustALtPhnNumber: '',
            CustomerEmail: '',
            ExpectedDeliveryDate: ''

        }]
    };

    var userRole=localStorage.getItem("UserRole");
    if (userRole==2){
    $http.post('GetAllAssignedPeoductDetailsByVendorId', {})
       .success(function (data, status, headers, config) {

           $scope.ProductDetails.items = data;

           angular.forEach(data, function (value, key) {
               if (data.length > 0) {
                   $scope.ProductDetails.items[key].ExpectedDeliveryDate = new Date(parseInt(value.ExpectedDeliveryDate.substr(6)));
               }

           });
       });

}
    else if (userRole == 1) {
        $http.post('GetAssignedProductDetailsByDeliveryBoyId', {})
     .success(function (data, status, headers, config) {

         $scope.ProductDetails.items = data;

         angular.forEach(data, function (value, key) {
             if (data.length > 0) {
                 $scope.ProductDetails.items[key].ExpectedDeliveryDate = new Date(parseInt(value.ExpectedDeliveryDate.substr(6)));
             }

         });
     });
    }
    else {
        alert("error");
    }


    $scope.GetProductDetailsByProductId = function (item) {
        $('#hdnProductId').val(item.Id);
        $('#txtProductName').val(item.ProductName);
        $('#txtCustomerAddress').val(item.CustomerAddress);
        $('#txtCustomerPin').val(item.CustomerPin);
        $('#txtCustomerPhoneNumber').val(item.CustomerPhoneNumber);
        $('#txtCustomerAltNo').val(item.CustALtPhnNumber);
        $("#txtEmail").val(item.CustomerEmail);
        $('#txtStatus').val(item.ProductStatus);
        $('#txtShippingCompanyName').val(item.ShippingCompanyName);
        $('#txtExpectedDelivery').val(item.ExpectedDeliveryDate);
        $("#txtShippingDate").val(item.LocalAddress);
        $("#txtPaymentStatus").val(item.PayemntStautus);
        $("#divProductDetails").modal('show');
    };
})