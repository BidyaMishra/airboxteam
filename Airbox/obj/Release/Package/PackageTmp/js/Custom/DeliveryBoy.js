﻿$(document).ready(function () {
    $("#btnAddDeliveryBoyDetails").on("click", function () {
        $("#divDeliveryBoyDetails").modal('show');
        ResetDeliveryBoy();
    });
    $("#btnAddDeliveryBoyProfile").on("click", function () {
        SaveDeliveryBoy();
    });
    $("#txtDBDOJ").datepicker({
        defaultDate: "",
        changeMonth: true,
        changeYear: true,
        dateFormat: "M dd, yy",
        yearRange: "1950:2050",
        minDate: '0'

    });
     GetDeliveryBoyDetailsList();
});

function GetDeliveryBoyDetailsList() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetAllDeliveryBoyDetailsList',
        success: function (res) {
            $('#tblDeliveryBoy tr').slice(1).remove();
            var trHTML = '';
            for (var i = 0; i < res.length; i++) {
                trHTML += '<tr><td>' + res[i].Id + '</td><td>' + res[i].Name + '</td><td>' + res[i].EmailId + '</td><td>' + res[i].PhoneNumber + '</td><td>' + res[i].AlternativePhonenmber + '</td><td>' + res[i].EducationDetails + '</td><td>' + res[i].VehicleNumber + '</td><td>' + res[i].DrivingLicenseNumber + '</td><td>' + res[i].PanNumber + '</td></tr>';
            }
            $('#tblDeliveryBoy').append(trHTML);
            InitiateDeliverBoygrid();


        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })


}

function SaveDeliveryBoy() {
    var objDeliveryBoyDetails = new DeliveryBoyDetails();
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'DeliveryBoyDashBoard/AddEditDeliveryBoyDetails',
        data: JSON.stringify({ 'DeliveryBoy': objDeliveryBoyDetails }),
        success: function (res) {
            if (res > 0) {
                alert('inserted')
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })

}

function DeliveryBoyDetails() {

    var objDeliveryBoyDetails = new Object();
    objDeliveryBoyDetails.FirstName = $("#txtDBFirstName").val();
    objDeliveryBoyDetails.LastName = $("#txtDBLastName").val();
    objDeliveryBoyDetails.AreaId = $("#txtDBAreaId").val();
    objDeliveryBoyDetails.EmailId = $("#txtDBEmailId").val();
    objDeliveryBoyDetails.PhoneNumber = $("#txtDBPhoneNumber").val();
    objDeliveryBoyDetails.AlternativePhonenmber = $("#txtAltPhoneNumber").val();
    objDeliveryBoyDetails.DateOfJoining = $("#txtDBDOJ").val();
    objDeliveryBoyDetails.EducationDetails = $("#txtDBEducationDetails").val();
    objDeliveryBoyDetails.VehicleNumber = $("#txtDBVehicleNumber").val();
    objDeliveryBoyDetails.DrivingLicenseNumber = $("#txtDBDl").val();
    objDeliveryBoyDetails.PanNumber = $("#txtDBPanNumber").val();
    objDeliveryBoyDetails.PinCodeId = $("#txtDBPinCode").val();
    objDeliveryBoyDetails.VendorId = $("#txtDBVendorId").val();
    objDeliveryBoyDetails.Password = $("#txtDBPassword").val();
    objDeliveryBoyDetails.GeoAreaMap = $("#txtDBMap").val();
    objDeliveryBoyDetails.RepositoryFile = $("#txtDBRepositoryFile").val();

    return objDeliveryBoyDetails;
}

function InitiateDeliverBoygrid() {
    var grid = $("#tblDeliveryBoy").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'

        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";

                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('.command-modify').each(function () {

        });

        grid.find(".command-modify").on("click", function (e) {
            var id = $(this).data("row-id");
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'GetDeliverBoyDetailsbyId',
                data: { 'Id': id },
                success: function (res) {
                    $('#hdndeliverboyId').val(res.Id)
                    $('#txtDBName').val(res.Name);
                    $('#txtDBAreaId').val(res.AreaId);
                    $('#txtDBEmailId').val(res.EmailId);
                    $('#txtDBPhoneNumber').val(res.PhoneNumber);
                    $('#txtAltPhoneNumber').val(res.AlternativePhonenmber);
                    $('#txtDBDOJ').val(GetFullDate(res.DateOfJoining));
                    $('#txtDBEducationDetails').val(res.EducationDetails);
                    $('#txtDBDl').val(res.DrivingLicenseNumber);
                    $('#txtDBVehicleNumber').val(res.VehicleNumber)
                    $('#txtDBPanNumber').val(res.PanNumber);
                    $('#txtDBPinCode').val(res.PinCodeId);
                    $('#txtDBVendorId').val(res.VendorId);
                    $('#txtDBPassword').val(res.Password);
                    $('#txtDBMap').val(res.GeoAreaMap);
                    $('#txtDBRepositoryFile').val(res.RepositoryFile);
                    $("#divDeliveryBoyDetails").modal('show');


                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })


        });
        grid.find(".command-delete").on("click", function (e) {
            //var customerId = $(this).data("row-id");
            alert('by')

        });
    });
}

function ResetDeliveryBoy() {

    $("#txtDBFirstName").val('');
    $("#txtDBLastName").val('');
    $("#txtDBAreaId").val('');
    $("#txtDBEmailId").val('');
    $("#txtDBPhoneNumber").val('');
    $("#txtAltPhoneNumber").val('');
    $("#txtDBDOJ").val('');
    $("#txtDBEducationDetails").val('');
    $("#txtDBVehicleNumber").val('');
    $("#txtDBDl").val('');
    $("#txtDBPanNumber").val('');
    $("#txtDBPinCode").val('');
    $("#txtDBVendorId").val('');
    $("#txtDBPassword").val('');
    $("#txtDBMap").val('');
    $("#ddlVendor").val(0);
    $("#ddlDistrict").val(0);
    $("#ddlArea").val(0);
    $("#ddlLocaity").val(0);
}