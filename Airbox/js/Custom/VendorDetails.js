﻿$(document).ready(function () {
    GetVendorDetailsList();

    $('#btnAddDeliveryBoyDetails').click(function () {
        $('#divVendorDetails').modal('show');
    });

    $('#btnAddVendorDetails').click(function () {
        if (ValidateVendorDetails()) {
            var vendorDetails = new Object();
            if ($('#hdnId').val() == '') {
                vendorDetails.Id = 0;
            }
            else {
                vendorDetails.Id = $('#hdnId').val();
            }
            vendorDetails.Name = $("#txtName").val();
            vendorDetails.Email = $("#txtEmail").val();
            vendorDetails.Password = $("#txtPassword").val();
            vendorDetails.PhoneNumber = $("#txtPhoneNumber").val();
            vendorDetails.AlternativePhoneNumber = $("#txtAltNo").val();
            vendorDetails.PinId = $("#txtpin").val();
            vendorDetails.Address = $("#txtAddress").val();
            vendorDetails.UserRoleId = 2;
            var dataToSend = JSON.stringify({ 'VendorDetails': vendorDetails });

            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: 'AddEditVendorDetails',
                data: dataToSend,
                success: function (res) {
                    if (res>0) {
                        ShowAlert(" Vendor Details Added", "Successfull! ", "success")
                        GetVendorDetailsList();
                     
                    }

                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })
        }
    })
});

function ValidateVendorDetails() {
    returnVal = true;

    returnVal = CheckEmptyOrNot($("#txtName")) && returnVal;
    returnVal = CheckEmptyOrNot($("#txtEmail")) && returnVal;
    returnVal = CheckEmptyOrNot($("#txtPassword")) && returnVal;
    returnVal = CheckEmptyOrNot($("#txtPhoneNumber")) && returnVal;
    returnVal = CheckEmptyOrNot($("#txtAltNo")) && returnVal;
    returnVal = CheckEmptyOrNot($("#txtpin")) && returnVal; 
    returnVal = CheckEmptyOrNot($("#txtAddress")) && returnVal;
  
    return returnVal;
}

function GetVendorDetailsList() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetVendorDetailsList',
        success: function (res) {
                $('#tblVendor tr').slice(1).remove();
                var trHTML = '';
                for (var i = 0; i < res.length; i++) {
                    trHTML += '<tr><td>' + res[i].Id + '</td><td>' + res[i].Email + '</td><td>' + res[i].PhoneNumber + '</td><td>' + res[i].AlternativePhoneNumber + '</td><td>' + res[i].Address + '</td><</tr>';
                }
                $('#tblVendor').append(trHTML);
                Initiategrid();
         

        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
               
  
}

function Initiategrid() {
    var grid = $("#tblVendor").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'
         
        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\"  data-row-Brr=\"" + row.CustomerBrr + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";
             
                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('.command-modify').each(function () {
        
        });
       
        grid.find(".command-modify").on("click", function (e) {
            alert('hi');
            //RemoveErrorFrompopup();
            //var customerId = $(this).data("row-id");
            //var customerPmi = $(this).data("row-pmi");
            //var customerBrr = $(this).data("row-Brr");
            //var effectiveFrom = $(this).data("row-Efr").trim();
            //var effectiveTo = $(this).data("row-Eft").trim();
            //var activateFrom = $(this).data("row-Actf").trim();
            //var activateTo = $(this).data("row-Actt").trim();
            //document.getElementById("txtCustomerName").value = $(this).closest('tr').find('td:eq(0)').text();
            //document.getElementById("txtCustomerBP").value = $(this).closest('tr').find('td:eq(1)').text();
   
        });
        grid.find(".command-delete").on("click", function (e) {
            //var customerId = $(this).data("row-id");
            alert('by')
         
        });
    });
}