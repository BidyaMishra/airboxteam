﻿var ProductDetailsModule = angular.module("ProductDetailsApp", [])

ProductDetailsModule.controller('ProductDetailsCntrl', function ($scope, $http, $sce, $filter, $timeout) {
    $scope.ProductDetails = {
        items: [{                                                                            
            Id: '',                                                                          
            ProductDetails: '',
            CustomerAddress: '',                                                             
            CustomerPin: '',                                                                 
            CustomerPhoneNumber: '',                                                         
            CustALtPhnNumber: '',                                                            
            CustomerEmail: '',                                                               
            ExpectedDeliveryDate: '' , 
            ShippingCompanyId:'',
            PSMID:'',
            PMID:'',
            ProductStatus:'',
            PayemntStautus:'',
            ShippingCompanyName:'',                                                          
        }]                                                                                   
        };                                                                                   
                                                                                             
        $scope.VendorList = {                                                                
        items: [{
            Id: '',
            Name: ''
        }]
    };

    $scope.ProductStatusMaster = {
        items: [{
            Id: '',
            ProductStatus: ''
        }]
    };

    $scope.ShippingCompanyMaster = {
        items: [{
            Id: '',
            ShippingCompanyName: '',
            PhoneNumber: '',
            HeadQuaterAddress: '',
            Email: '',
            LocalAddress: '',
            IsDeleted:''
        }]
    };

    $scope.PaymentStatusMaster = {
        items: [{
            Id: '',
            PayemntStautus: ''
        }]
    };

    //$scope.Productstatusfilter = function (searchresult) {
    //    debugger;
    //    if (searchresult.ProductStatus == $scope.ProductStatus)
    //    {
    //        return searchResult;
    //    }
          
    //}
    $scope.filter = {}
 
    $scope.Productbystatus = function (value) {
        // Display the wine if
        var displayWine = $scope.filter[value.ProductStatus] || noFilter($scope.filter);

        return displayWine;
    };

    //$scope.shippingNamefilter = {}
    //$scope.ProductbyShippingCompanyName = function (value) {
    //    debugger;
    //    var displayShippingCompanyName = $scope.filter[value.ShippingCompanyName] || noFilter($scope.filter);
    //    return displayShippingCompanyName;
    //}


    $scope.ViewProductDetails = function (value) {
       
        window.location = '/ViewProduct/index?ProductId=' + value.Id;

    }


    function noFilter(filterObj) {
        return Object.
          keys(filterObj).
          every(function (key) { return !filterObj[key]; });
    }
    $scope.ShippingDetails = function (xyz) {
        $scope.ShippingCompanyName = xyz;
    }
   
    $http.post('GetProductDetailsByAdmin', {})
       .success(function (data, status, headers, config) {

           $scope.ProductDetails.items = data;

           angular.forEach(data, function (value, key) {
               if (data.length > 0) {
                   $scope.ProductDetails.items[key].ExpectedDeliveryDate = new Date(parseInt(value.ExpectedDeliveryDate.substr(6)));
               }

           });
       });

    $http.post('GetVendorList', {})
    .success(function (data, status, headers, config) {

        $scope.VendorList.items = data;
    });


    $http.post('GetAllProductStatus', {})
   .success(function (data, status, headers, config) {

       $scope.ProductStatusMaster.items = data;
   });


    $http.post('GetAllShippingComapny', {})
   .success(function (data, status, headers, config) {

       $scope.ShippingCompanyMaster.items = data;
   });


    $http.post('GetAllPaymentStatusMaster', {})
   .success(function (data, status, headers, config) {

       $scope.PaymentStatusMaster.items = data;
   });

        $scope.GetProductDetailsByProductId = function (item) {
            $('#hdnProductId').val(item.Id);
            $('#txtProductName').val(item.ProductDetails);
            $('#txtCustomerAddress').val(item.CustomerAddress);
            $('#txtCustomerPin').val(item.CustomerPin);
            $('#txtCustomerPhoneNumber').val(item.CustomerPhoneNumber);
            $('#txtCustomerAltNo').val(item.CustALtPhnNumber);
            $("#txtEmail").val(item.CustomerEmail);
            $('#txtStatus').val(item.ProductStatus);
            $('#txtShippingCompanyName').val(item.ShippingCompanyName);
            $('#txtExpectedDelivery').val(item.ExpectedDeliveryDate);
            $("#txtShippingDate").val(item.LocalAddress);
            $("#txtPaymentStatus").val(item.PayemntStautus);
            $("#divProductDetails").modal('show');
        };
 
    $scope.FileUpload = function () {

        var attachmenturl;
        var res = new FormData();
        var files = $("#uploadFile").get(0).files;
        if (files.length > 0) {
            res.append("FileData", files[0]);
        }

        $.ajax({
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            async: false,
            dataType: 'JSON',
            url: '/ProductDetails/UploadFile',
            data: res,
            success: function (data) {
                attachmenturl = data;
            }
        });

        return attachmenturl;
    }
})

$(document).ready(function () {
    AssignProductToFranchise();
    //GetVendorDetailsList();
    // GetDeliveryBoyDetailsList();
    $("#btnAssignPin").on("click", function () {
        AssignProduct();
    });
    GetCompanyDetails();
  var RoleId=  $('#hdnUserRoleId').val()
  if (RoleId > 0)
    {
      if(RoleId=='3')
      {
          $('#liProductDetails').addClass('active')
          $('#liProductDetails>a').attr("aria-expanded", "true");
          $('#ProductDetails-info').addClass('active');
          $('#liDeliveryProduct').hide();
          $('#liUndeliveyProduct').hide();
          $('#liAssignproduct').removeClass('active');
          $('#Assignproduct').removeClass('active');

          
          UnAssignProductToFranchise();
      }
      if (RoleId == '2') {
          AssignProductToFranchise();
          $('#liProductDetails').removeClass('active');
          $('#liCompany').removeClass('active');
          $('#liCompany>a').attr("aria-expanded", "false");
          $('#liProductDetails>a').attr("aria-expanded", "false");
          $('#liProductDetails').hide();
          $('#liCompany').hide();
          $('#liDeliveryProduct').hide();
          $('#liUndeliveyProduct').hide();
          $('#liUnassigned').hide();
          $('#ProductDetails-info').removeClass('active');
          $('#liAssignproduct').addClass('active');
          $('#Assignproduct').addClass('active');
          $('#liAssignproduct>a').attr("aria-expanded", "true");
          
      }
      if (RoleId == '1') {
          AssignProductToFranchise();
          $('#liProductDetails').removeClass('active');
          $('#liProductDetails>a').attr("aria-expanded", "false");
          $('#liProductDetails').hide();
          $('#liUnassigned').hide();
          $('#liCompany').removeClass('active');
          $('#liCompany>a').attr("aria-expanded", "false");
          $('#liCompany').hide();
          $('#liAssignproduct').addClass('active');
          $('#liAssignproduct>a').attr("aria-expanded", "true");
          $('#Assignproduct').addClass('active');
      }
    }

    $("#btnAddVendorDetailsDetails").click(function () {
        RemoveErrorField();
        RemoveValueField();
        $("#divVendorDetails").modal('show');
    });

    $("#btnAddDeliveryBoyDetails").click(function () {
        $("#divDeliveryBoyDetails").modal('show');
    });

    $('#btnAddVendorDetails').click(function () {
        if (ValidateVendorDetails()) {
            var vendorDetails = new Object();
            if ($('#hdnId').val() == '') {
                vendorDetails.Id = 0;
            }
            else {
                vendorDetails.Id = $('#hdnId').val();
            }
            vendorDetails.Name = $("#txtName").val();
            vendorDetails.Email = $("#txtEmail").val();
            vendorDetails.Password = $("#txtPassword").val();
            vendorDetails.PhoneNumber = $("#txtPhoneNumber").val();
            vendorDetails.AlternativePhoneNumber = $("#txtAltNo").val();
            vendorDetails.PinId = $("#txtpin").val();
            vendorDetails.Address = $("#txtAddress").val();
            vendorDetails.UserRoleId = 2;
            var dataToSend = JSON.stringify({ 'VendorDetails': vendorDetails });

            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: 'AddEditVendorDetails',
                data: dataToSend,
                success: function (res) {
                    if (res > 0) {
                        $('#divVendorDetails').modal('hide');
                        ShowAlert(" Vendor Details Added", "Successfull! ", "success")
                   
                        //GetVendorDetailsList();
                        //$("#tblVendor").bootgrid("reload");
                        window.location.reload();

                    }

                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })
        }
    })

    $('#btnCalcelPopUp').click(function () {
        RemoveErrorField();
    })

    $("#btnAddDeliveryBoyProfile").click(function () {
        SaveDeliveryBoy();
    });

    $('#btnCompanyDetails').click(function () {
        RsetCompanyDetails();
        $("#divCompanyDetails").modal('show');
    });

    $('#btnAddCompanyDetails').click(function () {

        SaveCompanyDetails();
    });

    //$(".edit-pd").on("click", function () {
    //    var pid = (this.value);
    //    GetProductDetailsByProductId(pid)
    //});

   

    $("#txtDBDOJ").datepicker({
        defaultDate: "",
        changeMonth: true,
        changeYear: true,
        dateFormat: "M dd, yy",
        yearRange: "1950:2050",
        minDate: ''

    });

    $("#txtExpectedDelivery").datepicker({
        defaultDate: "",
        changeMonth: true,
        changeYear: true,
        dateFormat: "M dd, yy",
        yearRange: "1950:2050",
        minDate: ''

    });

    $("#txtShippingDate").datepicker({
        defaultDate: "",
        changeMonth: true,
        changeYear: true,
        dateFormat: "M dd, yy",
        yearRange: "1950:2050",
        minDate: ''

    });
    $("#gridProductDetails").hide();
    $("#productDisplay").on("click", function () {
        if ($("#productDisplay").text() == "Grid View") {
            $("#classicViewProductDetails").hide();
            $("#gridProductDetails").show();
            $("#productDisplay").text('Classic View');

        }
        else {
            $("#classicViewProductDetails").show();
            $("#gridProductDetails").hide();
            $("#productDisplay").text('Grid View');
        }
    });

    $("#btnAddProduct").click(function () {
        $("#divProductDetails").modal('show');
    });
    $("#btnAddProductDetails").click(function () {
        var productDetails = new Object();

        productDetails.ProductDetails = $("#txtProductName").val();
        productDetails.CustomerAddress = $("#txtCustomerAddress").val();
        productDetails.CustALtPhnNumber = $("#txtCustomerAltNo").val();
        productDetails.CustomerPin = $("#txtCustomerPin").val();
        productDetails.CustomerPhoneNumber = $("#txtCustomerPhoneNumber").val();
        productDetails.CustomerEmail = $("#txtEmail").val();
        productDetails.ProductStatus = $("#txtStatus").val();
        productDetails.PayemntStautus = $("#txtPaymentStatus").val();
        productDetails.ShippingCompanyName = $("#txtShippingCompanyName").val();
        productDetails.ExpectedDeliveryDate = $("#txtExpectedDelivery").val();
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: 'AddEditProductByAdmin',
            data: JSON.stringify({ 'product': productDetails }),
            success: function (res) {
                if (res > 0) {
                    $("#divProductDetails").modal('hide');

                }
                else {
                }
            },
            error: function (type) { alert("ERROR!!" + type.responseText); }
        })
    });

});

function ValidateVendorDetails() {
    returnVal = true;

    returnVal = IsNotBlank($("#txtName")) && returnVal;
    returnVal = IsNotBlank($("#txtEmail")) && returnVal;
    returnVal = IsNotBlank($("#txtPassword")) && returnVal;
    returnVal = IsNotBlank($("#txtPhoneNumber")) && returnVal;
    returnVal = IsNotBlank($("#txtAltNo")) && returnVal;
    returnVal = IsNotBlank($("#txtpin")) && returnVal;
    returnVal = IsNotBlank($("#txtAddress")) && returnVal;

    return returnVal;
}

function GettblAssignProduct() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GettblAssignProduct',
        success: function (res) {
            $('#tblVendor tr').slice(1).remove();
            var trHTML = '';
            for (var i = 0; i < res.length; i++) {
                trHTML += '<tr><td>' + res[i].Id + '</td><td>' + res[i].Name + '</td><td>' + res[i].Email + '</td><td>' + res[i].PhoneNumber + '</td><td>' + res[i].AlternativePhoneNumber + '</td><td>' + res[i].Address + '</td></tr>';
            }
            $('#tblVendor').append(trHTML);
            InitiateVendorgrid();


        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })


}

function UnAssignProductToFranchise() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetAllUnAssignProductToAdmin',
        success: function (res) {
            $('#tblUnAssignProduct tr').slice(1).remove();
            var trHTML = '';
            for (var i = 0; i < res.length; i++) {
                trHTML += '<tr><td>' + res[i].Id + '</td><td>' + res[i].ProductId + '</td><td>' + res[i].CustomerPin + '</td><td>' + res[i].CustomerPhoneNumber +
                    '</td><td>' + res[i].PayemntStautus + '</td><td>' + res[i].ShippingCompanyName + '</td><td>' + res[i].Cost + '</td><td>' + GetFullDate(res[i].ExpectedDeliveryDate) + '</td></tr>';
            }
         
            $('#tblUnAssignProduct').append(trHTML);
            InitiatetblUnAssignProduct();


        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function InitiatetblUnAssignProduct() {
    var grid = $("#tblUnAssignProduct").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'

        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";

                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
     
        grid.find(".command-modify").on("click", function (e) {
            var Id = $(this).data("row-id");
            $("#divAssignPin").modal('show');
            $("#hdnProductId").val(Id);
            



        });
        grid.find(".command-delete").on("click", function (e) {
            //var customerId = $(this).data("row-id");
            alert('by')

        });
    });
}

function InitiateVendorgrid() {
    var grid = $("#tblVendor").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'

        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";

                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('.command-modify').each(function () {

        });

        grid.find(".command-modify").on("click", function (e) {
            var id = $(this).data("row-id");
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'GetVendorDetailsbyId',
                data: {'Id':id},
                success: function (res) {
                    $('#hdnId').val(res.Id)
                    $('#txtName').val(res.Name);
                    $('#txtEmail').val(res.Email);
                    $('#txtPhoneNumber').val(res.PhoneNumber);
                    $('#txtAltNo').val(res.AlternativePhoneNumber);
                    $('#txtpin').val(res.PinId);
                    $('#txtPassword').val(res.Password);
                    $('#txtAddress').val(res.Address);
                    $("#divVendorDetails").modal('show');

                   
                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })


        });
   
    });
}



function SaveDeliveryBoy() {
    var objDeliveryBoyDetails = new DeliveryBoyDetails();
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'AddEditDeliveryBoyDetails',
        data: JSON.stringify({ 'DeliveryBoy': objDeliveryBoyDetails }),
        success: function (res) {
            if (res > 0) {
                $('#divDeliveryBoyDetails').modal('hide');
                ShowAlert(" DeliveryBoy Details Added", "Successfull! ", "success")
                window.location.reload();
               // GetDeliveryBoyDetailsList();
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })

}

function DeliveryBoyDetails() {

    var objDeliveryBoyDetails = new Object();
    
    if ($('#hdndeliverboyId').val() == '') {
        objDeliveryBoyDetails.Id = 0;
    }
    else {
        objDeliveryBoyDetails.Id = $('#hdndeliverboyId').val();
    }
    objDeliveryBoyDetails.Name = $("#txtDBName").val();
    objDeliveryBoyDetails.AreaId = $("#txtDBAreaId").val();
    objDeliveryBoyDetails.EmailId = $("#txtDBEmailId").val();
    objDeliveryBoyDetails.PhoneNumber = $("#txtDBPhoneNumber").val();
    objDeliveryBoyDetails.AlternativePhonenmber = $("#txtAltPhoneNumber").val();
    objDeliveryBoyDetails.DateOfJoining = $("#txtDBDOJ").val();
    objDeliveryBoyDetails.EducationDetails = $("#txtDBEducationDetails").val();
    objDeliveryBoyDetails.VehicleNumber = $("#txtDBVehicleNumber").val();
    objDeliveryBoyDetails.DrivingLicenseNumber = $("#txtDBDl").val();
    objDeliveryBoyDetails.PanNumber = $("#txtDBPanNumber").val();
    objDeliveryBoyDetails.PinCodeId = $("#txtDBPinCode").val();
    objDeliveryBoyDetails.VendorId = $("#txtDBVendorId").val();
    objDeliveryBoyDetails.Password = $("#txtDBPassword").val();
    objDeliveryBoyDetails.GeoAreaMap = $("#txtDBMap").val();
    objDeliveryBoyDetails.RepositoryFile = $("#txtDBRepositoryFile").val();

    return objDeliveryBoyDetails;
}

function GetCompanyDetails() {

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: 'GetCompanyDetails',
            success: function (res) {
                if (res.length > 0) {
                    $("#tblCompany").empty();
                    var trHTML = '';
                    for (var i = 0; i < res.length; i++)  {
                        var priceval = res[i];
                        trHTML += "<tr><td>" + res[i].ShippingCompanyName + "</td><td>" + res[i].Email + "</td><td>" + res[i].PhoneNumber + "</td><td>" + res[i].HeadQuaterAddress + "</td><td>" + res[i].LocalAddress + "</td><td>" + "<button type='button' onclick='GetCompanyDetailsbyId(" + res[i].Id + ");' class='btn btn-primary grd-button m-r-6 company-grid-action'><span class='glyphicon glyphicon-pencil'></span></button>" + "<button type='button' onclick='DeleteCompanyDetailsbyId(" + res[i].Id + ");' class='btn btn-danger grd-button company-grid-action'><span class='glyphicon glyphicon-trash'></span></button>" + "</td></tr>";
                    }
                    $("#tblCompany").append(trHTML);
                }
                else {
                    $("#tblParcelView").hide();
                    $("#headShow").show();
                }
            },
            error: function (type) { alert("ERROR!!" + type.responseText); }
        })
}

function SaveCompanyDetails() {
    var cmpdetails = new CompanyData();
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'AddEditCompanyDetails',
        data: JSON.stringify({ 'CompanyDetails': cmpdetails }),
        success: function (res) {
                ShowAlert(" Company Details Added", "Successfull! ", "success")
                $("#divCompanyDetails").modal('hide');
                GetCompanyDetails();          
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function CompanyData() {
    var cmpdetails = new Object();
    if ($('#hdnCompanyId').val() == '') {
        cmpdetails.Id = 0;
    }
    else {
        cmpdetails.Id = $('#hdnCompanyId').val();
    }
    cmpdetails.ShippingCompanyName = $('#txtCmpCompanyName').val();
    cmpdetails.Email = $('#txtCmpEmail').val();
    cmpdetails.PhoneNumber = $('#txtCmpPhoneNo').val();
    cmpdetails.HeadQuaterAddress = $('#txtCmpHeadquarter').val();
    cmpdetails.LocalAddress = $('#txtCmpLocalAddress').val();
    return cmpdetails;
}

function GetCompanyDetailsbyId(id)
{
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetCompanyDetailsbyId',
        data: JSON.stringify({ 'Id': id }),
        success: function (res) {
            if (res.Id > 0) {
            
                $('#hdnCompanyId').val(res.Id);
                $('#txtCmpCompanyName').val(res.ShippingCompanyName);
                $('#txtCmpEmail').val(res.Email);
                $('#txtCmpPhoneNo').val(res.PhoneNumber);
                $('#txtCmpHeadquarter').val(res.HeadQuaterAddress);
                $('#txtCmpLocalAddress').val(res.LocalAddress);
                $("#divCompanyDetails").modal('show');
            }
            else {
            }
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
  
}

function DeleteCompanyDetailsbyId(id) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'DeleteCompanyDetailsbyId',
        data: JSON.stringify({ 'Id': id }),
        success: function (res) {
       
                ShowAlert("Company Details Deleted", "Successfull! ", "success")
                GetCompanyDetails();
              
          
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function RsetCompanyDetails()
{
    $('#hdnCompanyId').val('');
    $('#txtCmpCompanyName').val('');
    $('#txtCmpEmail').val('');
    $('#txtCmpPhoneNo').val('');
    $('#txtCmpHeadquarter').val('');
    $('#txtCmpLocalAddress').val('');
}

function RemoveErrorField() {
    RemoveErrorFromTextFields($("#txtName"));
    RemoveErrorFromTextFields($("#txtEmail"));
    RemoveErrorFromTextFields($("#txtPassword"));
    RemoveErrorFromTextFields($("#txtPhoneNumber"));
    RemoveErrorFromTextFields($("#txtAltNo"));
    RemoveErrorFromTextFields($("#txtpin"));
    RemoveErrorFromTextFields($("#txtAddress"));

}

function RemoveValueField() {
   $("#txtName").val('');
   $("#txtEmail").val('');
   $("#txtPassword").val('');
   $("#txtPhoneNumber").val('');
   $("#txtAltNo").val('');
   $("#txtpin").val('');
   $("#txtAddress").val('');

}

function AssignProductToFranchise() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetFranchiseAssignProduct',
        success: function (res) {
            $('#tblAssignProduct tr').slice(1).remove();
            var trHTML = '';
            for (var i = 0; i < res.length; i++) {
                trHTML += '<tr><td>' + res[i].Id + '</td><td>' + res[i].ProductId + '</td><td>' + res[i].DeliveryBoyName + '</td><td>' + res[i].DeliveryBoyMobileNo +
                    '</td><td>' + res[i].PayemntStautus + '</td><td>' + res[i].ShippingCompanyName + '</td><td>' + GetFullDate(res[i].ExpectedDeliveryDate) + '</td></tr>';
            }
            $('#tblAssignProduct').append(trHTML);
            InitiateAssignProduct();


        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function InitiateAssignProduct() {
    var grid = $("#tblAssignProduct").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'

        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";

                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('.command-modify').each(function () {

        });

        grid.find(".command-modify").on("click", function (e) {
            var Id = $(this).data("row-id");
            window.location = '/ViewProduct/index?ProductId=' + Id;
        });
       
    });
}

function AssignProduct() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'AssignProduct',
        data: JSON.stringify({ 'productId': $("#hdnProductId").val(), 'pinId': $("#cbPinVal").val(), 'pinVal': $("#lblPinVal").text() }),
        success: function (res) {
            if(res>0)
            {
                $('#divAssignPin').modal('hide');
                UnAssignProductToFranchise();
            }

        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}