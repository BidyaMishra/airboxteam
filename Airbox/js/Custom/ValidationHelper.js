﻿function CheckEmail(email) {

    var returnVal = false;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;


    if (!filter.test(email.val())) {
        email.addClass('errorClass');
        email.focus;
    }
    else {
        email.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}

function CheckPassword(pwd) {

    var returnVal = false;
    var pwdregex = /^([a-zA-Z0-9@*#]{6,15})$/;

    if (!pwdregex.test(pwd.val())) {
        pwd.addClass('errorClass');
        pwd.focus;
    }
    else {
        pwd.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}

function CheckAlphaBet(AlphaBet) {

    var returnVal = false;
    var alphabetic = /^[A-Za-z ]+$/;

    if (!alphabetic.test(AlphaBet.val())) {
        AlphaBet.addClass('errorClass');
        AlphaBet.focus;
    }
    else {
        AlphaBet.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}

function CheckPhoneValidation(PhoneId) {

    var returnVal = false;
    var regexPhone = /^[789]\d{9}$/;

    if (!regexPhone.test(PhoneId.val())) {
        PhoneId.addClass('errorClass');
        PhoneId.focus;
    }
    else {
        PhoneId.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}

function checkNameStrength(value) {
    var returnVal = false;

    if (value.val().length > 2 && value.val().length < 200) {
        value.removeClass('errorClass');
        returnVal = true;
    }
    else{
        value.addClass('errorClass');
        value.focus;
    }
    return returnVal;
}


function CheckEmptyOrNot(id) {

    var returnVal = false;

    if (id.val().trim().length == 0) {
        id.addClass('errorClass');
        id.focus;
    }
    else {
        id.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}


function CheckDecimalValue(decimalVal) {

    var returnVal = false;
    var regexPhone = /^\d+(\.\d{1,2})?$/;

    if (!regexPhone.test(decimalVal.val())) {
        decimalVal.addClass('errorClass');
        decimalVal.focus;
    }
    else {
        decimalVal.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}
function CheckAlphaNumeric(alphNumericVal) {

    var returnVal = false;
    var regexAlphanUmeric = /^[a-z0-9]+$/;

    if (!regexAlphanUmeric.test(alphNumericVal.val())) {
        alphNumericVal.addClass('errorClass');
        alphNumericVal.focus;
    }
    else {
        alphNumericVal.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}


function CheckIndianPostalCode(postalCode) {

    var returnVal = false;
    var regexPostalCode = /^[1-9][0-9]{5}$/;

    if (!regexPostalCode.test(postalCode.val())) {
        postalCode.addClass('errorClass');
        postalCode.focus;
    }
    else {
        postalCode.removeClass('errorClass');
        returnVal = true;
    }

    return returnVal;
}



