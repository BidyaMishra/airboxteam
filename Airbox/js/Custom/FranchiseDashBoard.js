﻿$(document).ready(function () {
    GetUserMaritalStatusList();
  GetFranchiseProfileDetailsbyId();
    GetUserGenderList();
    $('#dvAllFranchiseList').hide();
    var roleId = $('#hdnRoleId').val();
    if (roleId == "2")
    {
        $('#liAllFranchiseList').hide();
        $('#liFranchiseProfile').addClass('active');
        $('#liFranchiseProfile>a').attr("aria-expanded", "true");
        $('#FranchiseProfile').addClass('active');
        $('#AllFranchiseList').removeClass('active');
    }

    GetVendorDetailsList();
    $("#btnAddVendorDetailsDetails").click(function () {
        RemoveErrorField();
        RemoveValueField();
        $("#divVendorDetails").modal('show');
    });

    $('#btnAddVendorDetails').click(function () {
        if (ValidateVendorDetails()) {
            var vendorDetails = new Object();
            if ($('#hdnId').val() == '') {
                vendorDetails.Id = 0;
            }
            else {
                vendorDetails.Id = $('#hdnId').val();
            }
            vendorDetails.FirstName = $("#txtFirstName").val();
            vendorDetails.LastName = $("#txtLastName").val();
            vendorDetails.Email = $("#txtEmail").val();
            vendorDetails.Password = $("#txtPassword").val();
            vendorDetails.PhoneNumber = $("#txtPhoneNumber").val();
            vendorDetails.AlternativePhoneNumber = $("#txtAltNo").val();
            vendorDetails.PinId = $("#txtpin").val();
            vendorDetails.Address = $("#txtAddress").val();
            vendorDetails.UserRoleId = 2;
            var dataToSend = JSON.stringify({ 'VendorDetails': vendorDetails });
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: 'AddEditVendorDetails',
                data: dataToSend,
                success: function (res) {
                    if (res > 0) {
                        $('#divVendorDetails').modal('hide');
                        ShowAlert(" Vendor Details Added", "Successfull! ", "success")
                      
                        //GetVendorDetailsList();
                        //$("#tblVendor").bootgrid("reload");
                        window.location.reload();
                    }
                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })
        }
    })
});
function GetVendorDetailsList() {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: 'GetVendorDetailsList',
        success: function (res) {
            $('#tblVendor tr').slice(1).remove();
            var trHTML = '';
            for (var i = 0; i < res.length; i++) {
                trHTML += '<tr><td>' + res[i].UserRelationId + '</td><td>' + res[i].Name + '</td><td>' + res[i].Email + '</td><td>' + res[i].PhoneNumber + '</td><td>' + res[i].AlternativePhoneNumber + '</td><td>' + res[i].Address + '</td></tr>';
            }
            $('#tblVendor').append(trHTML);
            InitiateVendorgrid();
            $('#dvAllFranchiseList').show();
        },
        error: function (type) { alert("ERROR!!" + type.responseText); }
    })
}

function InitiateVendorgrid() {
    var grid = $("#tblVendor").bootgrid({
        css: {
            icon: 'glyphicon',
            iconColumns: 'glyphicon glyphicon-filter',
            iconDown: 'zmdi-chevron-down',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-chevron-up'

        },
        selection: false,
        multiSelect: false,
        rowSelect: false,
        keepSelection: false,
        caseSensitive: false,
        formatters: {
            "commands": function (column, row) {
                var buttonHtml = "<i data-row-id=\"" + row.id + "\" class=\"zmdi zmdi-edit zmdi-hc-2x c-blue m-r-10 command-modify\" title=\"Modify Customer\"></i> ";

                return buttonHtml;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('.command-modify').each(function () {

        });
        grid.find(".command-modify").on("click", function (e) {
            var relationId = $(this).data("row-id");
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'GetFranchiseProfileDetailsbyFranchiesRelationId',
                data: { 'userRelationIdVal': relationId },
                success: function (res) {
                    $('#hdnId').html(res.Id)
                    $('#ddGender').html(res.Gender);
                    $('#ddDOB').html(GetFullDate(res.DOB));
                    $('#ddMaritalStatus').html(res.MaritalStatus);
                    $('#ddAddress').html(res.Address);
                    $('#ddpin').html(res.PinId);
                    $('#ddMobile').html(res.Password);
                    $('#ddaltrMobile').html(res.AlternativePhoneNumber);
                    $("#ddEmail").html(res.Email);
                    $("#ddFaceBook").html(res.FaceBook);
                    $("#ddCity").html(res.Password);
                    $("#ddFulName").html(res.Name);
                    $('#liFranchiseProfile').addClass('active'); 
                    $('#liFranchiseProfile>a').attr("aria-expanded", "true");
                    $('#FranchiseProfile').addClass('active');

                    

                    $('#liAllFranchiseList').removeClass('active');
                    $('#liAllFranchiseList>a').attr("aria-expanded", "false");
                    $('#AllFranchiseList').removeClass('active');
                },
                error: function (type) { alert("ERROR!!" + type.responseText); }
            })
        });
       
    });
}

function RemoveErrorField() {
    RemoveErrorFromTextFields($("#txtFirstName"));
    RemoveErrorFromTextFields($("#txtLastName"));
    RemoveErrorFromTextFields($("#txtEmail"));
    RemoveErrorFromTextFields($("#txtPassword"));
    RemoveErrorFromTextFields($("#txtPhoneNumber"));
    RemoveErrorFromTextFields($("#txtAltNo"));
    RemoveErrorFromTextFields($("#txtpin"));
    RemoveErrorFromTextFields($("#txtAddress"));
}

function RemoveValueField() {
    $("#txtFirstName").val('');
    $("#txtLastName").val('');
    $("#txtEmail").val('');
    $("#txtPassword").val('');
    $("#txtPhoneNumber").val('');
    $("#txtAltNo").val('');
    $("#txtpin").val('');
    $("#txtAddress").val('');
}

function ValidateVendorDetails() {
    returnVal = true;
    returnVal = IsNotBlank($("#txtFirstName")) && returnVal;
    eturnVal = IsNotBlank($("#txtLastName")) && returnVal;
    returnVal = IsNotBlank($("#txtEmail")) && returnVal;
    returnVal = IsNotBlank($("#txtPassword")) && returnVal;
    returnVal = IsNotBlank($("#txtPhoneNumber")) && returnVal;
    returnVal = IsNotBlank($("#txtAltNo")) && returnVal;
    returnVal = IsNotBlank($("#txtpin")) && returnVal;
    returnVal = IsNotBlank($("#txtAddress")) && returnVal;
    return returnVal;
}

function GetUserMaritalStatusList() {
    $.ajax({
        type: "POST",
        datatype: "json",
        url: "GetUserMaritalStatusList",
        success: function (res) {
            $("#ddlMaritalStatus option:not([value='0'])").remove();
                for (var i = 0; i < res.length; i++) {
                    var newOption = $('<option  value="' + res[i].MaritalStatusId + '">' + res[i].MaritalStatus + '</option>');
                    $('#ddlMaritalStatus').append(newOption);
                }

        }
    })

}
function GetFranchiseProfileDetailsbyId() {
    $.ajax({
        type: "POST",
        datatype: "json",
        url: "GetFranchiseProfileDetailsbyId",
        //data:{'UserId':Id},
        success: function (res) {
            BindProfileDetails(res);
        }
    })
}
function GetUserGenderList() {
    $.ajax({
        type: "POST",
        datatype: "json",
        url: "GetUserGenderList",
        success: function (res) {
            for (var i = 0; i < res.length; i++) {
                debugger;
                var radioVal="<label class='radio radio-inline m-r-10'>"+
               "<input type='radio' name='rdngender' value='" + res[i].GenderId + "'>" +
           "<i class='input-helper'></i>" + res[i].Gender + "</label>";
                $("#dvgender").append(radioVal);
            }
           
        }
    })
}
function BindProfileDetails(res) {
    $("#ddFulName").html(res.Name);
    $("#ddGender").html(res.Gender);
    $("#ddDOB").html(GetFullDate(res.DOB));
    $("#ddMaritalStatus").html(res.MaritalStatus);
    $("#ddAddress").html(res.Address);
    $("#ddpin").html(res.PinId);

    $("#ddMobile").html(res.PhoneNumber);
    $("#ddaltrMobile").html(res.AlternativePhoneNumber);
    $("#ddEmail").html(res.Email);
    $("#ddEmail").html(res.ddFaceBook);
    


    
    $("#txtFirstName").val(res.FirstName);
    $("#txtLastName").val(res.LastName);
    $("#ddlMaritalStatus").val(res.MaritalStatusId);
    $("#txtCity").val(res.City);
    $("#txtDOB").val(GetFullDate(res.DOB));
    $("#txtCurrentAddress").val(res.Address);


    $("#txtMobile1").val(res.PhoneNumber);
    $("#txtMobile2").val(res.AlternativePhoneNumber);
    $("#txtEmail").val(res.Email);
    $("#txtFacebook").val(res.FaceBook);


}
    