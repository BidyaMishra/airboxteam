﻿var assignProductDetailsModule = angular.module("assignProductDetailsApp", [])

assignProductDetailsModule.controller('assignProductDetailsCntrl', function ($scope, $http, $sce, $filter, $timeout) {
    $scope.ProductDetails = {
        items: [{
            Id: '',
            ProductDetails: '',
            CustomerAddress: '',
            CustomerPin: '',
            CustomerPhoneNumber: '',
            CustALtPhnNumber: '',
            CustomerEmail: '',
            ExpectedDeliveryDate: '',
            ShippingCompanyId: '',
            PSMID: '',
            PMID: '',
            ProductStatus: '',
            PayemntStautus: '',
            ShippingCompanyName: '',
        }]
    };



    $http.post('GetProductDetailsByAdmin', {})
       .success(function (data, status, headers, config) {

           $scope.ProductDetails.items = data;

           angular.forEach(data, function (value, key) {
               if (data.length > 0) {
                   $scope.ProductDetails.items[key].ExpectedDeliveryDate = new Date(parseInt(value.ExpectedDeliveryDate.substr(6)));
               }

           });
       });


})