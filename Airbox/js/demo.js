$(document).ready(function () {

    var c = parseInt($("#hdnMasterRoleId").val());
    var isUserApproved = $("#hdnIsUserApproved").val();
    switch (c) {
        case 1:

            $("#liEmployerMenu").hide();
            $("#liAdminMenu").hide();
            break;
        case 2:
            $("#liEmployeeMenu").hide();
            $("#liAdminMenu").hide();
            if (!stringToBoolean(isUserApproved))
            {
                $("#liEmployerMenuEmployerDashBoard").hide();
                $("#liEmployerMenuAddJob").hide();
            }
            break;
        case 3:
            $("#liEmployeeMenu").hide();
            $("#liEmployerMenu").hide();
            break;
    }
    //Welcome Message (not for login page)
    function notify(message, type){
        $.growl({
            message: message
        },{
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 2500,
            animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
            },
            offset: {
                x: 20,
                y: 85
            }
        });
    };
    
    if (!$('.login-content')[0]) {
        notify('Welcome back Mallinda Hollaway', 'inverse');
    } 
});