﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class DeliveryBoyDashBoardController : Controller
    {
        //
        // GET: /DeliveryBoyDashBoard/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult AddEditDeliveryBoyDetails(DeliveryBoy deliveryBoy)
        {
            int affectedId = 0;
            DeliveryBoyBusinessLayer deliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            affectedId = deliveryBoyBusinessLayer.AddEditDeliveryBoyDetails(deliveryBoy);

            return Json(affectedId, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllDeliveryBoyDetailsList()
        {
            DeliveryBoyBusinessLayer _ObjDeliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            List<DeliveryBoy> DeliveryboyDetails = _ObjDeliveryBoyBusinessLayer.GetAllDeliveryBoyDetailsList();
            return Json(DeliveryboyDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAlDistrictByDistrictName()
        {
            PinBusinessLayer _objPinBusinessLayer = new PinBusinessLayer();
            List<MasterDistrict> lstMasterDistrict = _objPinBusinessLayer.GetAlDistrictByDistrictName();
            return Json(lstMasterDistrict, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllAreaByAreaNameAndDistrictId(int districtId)
        {
            PinBusinessLayer _objPinBusinessLayer = new PinBusinessLayer();
            List<MasterArea> latMasterArea = _objPinBusinessLayer.GetAllAreaByAreaNameAndDistrictId(districtId);
            return Json(latMasterArea, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllLocalAreaByLocalAreaNameAndAreaId(int areaId)
        {
            PinBusinessLayer _objPinBusinessLayer = new PinBusinessLayer();
            List<MasterLocality> lstMasterLocality = _objPinBusinessLayer.GetAllLocalAreaByLocalAreaNameAndAreaId(areaId);
            return Json(lstMasterLocality, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllPinByLocalAreaId(int localAreaId)
        {
            PinBusinessLayer _objPinBusinessLayer = new PinBusinessLayer();
            MasterPin lstMasterPin = _objPinBusinessLayer.GetAllPinByLocalAreaId(localAreaId);
            return Json(lstMasterPin, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVendorDetailsList()
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            List<Vendor> vendorDetails = _objVendorBusinessLayer.GetVendorDetailsList();
            return Json(vendorDetails, JsonRequestBehavior.AllowGet);
        }
	}
}