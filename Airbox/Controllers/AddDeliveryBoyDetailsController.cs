﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class AddDeliveryBoyDetailsController : Controller
    {
        // GET: AddDeliveryBoyDetails
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditDeliveryBoyDetails(DeliveryBoy deliveryBoy)
        {
            int affectedId = 0;
            DeliveryBoyBusinessLayer deliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            affectedId = deliveryBoyBusinessLayer.AddEditDeliveryBoyDetails(deliveryBoy);

            return Json(affectedId, JsonRequestBehavior.AllowGet);
        }
    }
}