﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class ViewProductController : Controller
    {
        //
        // GET: /ViewProduct/
        public ActionResult Index()
        {
            UserBusinessLayer _objUserBusinessLayer = new UserBusinessLayer();
            ProductViewModel productViewModel = new ProductViewModel();
            int Id=Convert.ToInt32(Request.QueryString["ProductId"]);
            //int Id = 9;
            productViewModel.ProductItem=_objUserBusinessLayer.GetProductDetailsbyId(Id);
            return View(productViewModel);
        }
	}
}