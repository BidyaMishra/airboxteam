﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            TempData["RoleId"] = AirBoxSession.User.UserRoleId;
            return View();
        }
    }
}