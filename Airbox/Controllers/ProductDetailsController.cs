﻿using Airbox.Business;
using AirBox.Model.Model;
using AirBox.Model.ViewModel;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Configuration;
using Airbox.SessionoutFilter;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class ProductDetailsController : Controller
    {
        readonly AdminBusinessLayer _objAdminBusinessLayer = new AdminBusinessLayer();
        //
        // GET: /ProductDetails/
        public ActionResult Index()
        {
            TempData["RoleId"] = AirBoxSession.User.UserRoleId;
            return View();
        }
        public ActionResult GetProductDetailsByAdmin()
        {
            List<Product> ProductDetailsList = _objAdminBusinessLayer.GetProductDetailsByAdmin();
            return Json(ProductDetailsList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorList()
        {
            List<Vendor> ProductDetailsList = _objAdminBusinessLayer.GetVendorList();
            return Json(ProductDetailsList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult UploadFile()
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                //var file = System.Web.HttpContext.Current.Request.Files["EmployeeDocument"];
                if (file != null && file.ContentLength > 0)
                {

                    var fileName = Path.GetFileName(file.FileName);

                    //string folderPath = WebConfigurationManager.AppSettings[Constants.ConfigKeys.FileUploadPath];
                    string path = ("~/Docs/");

                    string filePath = Server.MapPath(path + fileName);
                    file.SaveAs(filePath);
                    SaveAndImportExcelFile(filePath);
                }
            }
            return View("Index");
        }


        private void SaveAndImportExcelFile(string filePath)
        {
            ReportBusinessLayer reportBusinessLayer = new ReportBusinessLayer();

            DataTable excelData = FetchDatatableFromExcelFile(filePath);

            List<string> accounts = reportBusinessLayer.AddEditProduct(excelData);
        }


        public DataTable FetchDatatableFromExcelFile(string excelFilePath)
        {
            string extension = Path.GetExtension(excelFilePath);
            FileStream stream = System.IO.File.Open(excelFilePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;
            switch (extension)
            {
                case ".xls":
                    //Excel 97-03 
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx":
                    //Excel 07 
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            try
            {
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();
                DataTable dtResult = result.Tables[0];

                excelReader.Close();

                return dtResult;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public ActionResult AddEditVendorDetails(Vendor vendorDetails)
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            int result = _objVendorBusinessLayer.AddEditVendorDetails(vendorDetails);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorDetailsList()
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            List<Vendor> vendorDetails = _objVendorBusinessLayer.GetVendorDetailsList();
            return Json(vendorDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEditDeliveryBoyDetails(DeliveryBoy deliveryBoy)
        {
            int affectedId = 0;
            DeliveryBoyBusinessLayer _ObjDeliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            affectedId = _ObjDeliveryBoyBusinessLayer.AddEditDeliveryBoyDetails(deliveryBoy);

            return Json(affectedId, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllDeliveryBoyDetailsList()
        {
            DeliveryBoyBusinessLayer _ObjDeliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            List<DeliveryBoy> DeliveryboyDetails = _ObjDeliveryBoyBusinessLayer.GetAllDeliveryBoyDetailsList();
            return Json(DeliveryboyDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorDetailsbyId(int Id)
        {
            VendorBusinessLayer _ObjVendorBusinessLayer = new VendorBusinessLayer();
            Vendor vederDetails = _ObjVendorBusinessLayer.GetVendorDetailsbyId(Id);
            return Json(vederDetails, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetDeliverBoyDetailsbyId(int Id)
        {
            DeliveryBoyBusinessLayer _ObjDeliveryBoyBusinessLayer = new DeliveryBoyBusinessLayer();
            DeliveryBoy deliveryBoy = _ObjDeliveryBoyBusinessLayer.GetDeliverBoyDetailsbyId(Id);
            return Json(deliveryBoy, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCompanyDetails()
        {
            AdminBusinessLayer _ObjAdminBusinessLayer = new AdminBusinessLayer();
            List<Company> companyDetails = _ObjAdminBusinessLayer.GetCompanyDetails();
            return Json(companyDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteCompanyDetailsbyId(int Id)
        {
            AdminBusinessLayer _ObjAdminBusinessLayer = new AdminBusinessLayer();
            int Result = _ObjAdminBusinessLayer.DeleteCompanyDetailsbyId(Id);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEditCompanyDetails(Company CompanyDetails)
        {
            AdminBusinessLayer _ObjAdminBusinessLayer = new AdminBusinessLayer();
            _ObjAdminBusinessLayer.AddEditCompanyDetails(CompanyDetails);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCompanyDetailsbyId(int Id)
        {
            AdminBusinessLayer _ObjAdminBusinessLayer = new AdminBusinessLayer();
            Company companydetails = _ObjAdminBusinessLayer.GetCompanyDetailsbyId(Id);
            return Json(companydetails, JsonRequestBehavior.AllowGet);
        }



        public ActionResult AddEditProductByAdmin(Product product)
        {
            int productId = _objAdminBusinessLayer.AddEditProductByAdmin(product);
            return Json(productId, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetProductDetailsByProductId(int productId)
        {
            Product product = _objAdminBusinessLayer.GetProductDetailsByProductId(productId);
            return Json(product, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllProductStatus()
        {
            List<ProductStatusMaster> lstProductStatusMaster = _objAdminBusinessLayer.GetAllProductStatus();
            return Json(lstProductStatusMaster, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllShippingComapny()
        {
            List<ShippingCompanyMaster> lstShippingCompanyMaster = _objAdminBusinessLayer.GetAllShippingComapny();
            return Json(lstShippingCompanyMaster, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllPaymentStatusMaster()
        {
            List<PaymentStatusMaster> lstPaymentStatusMaster = _objAdminBusinessLayer.GetAllPaymentStatusMaster();
            return Json(lstPaymentStatusMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFranchiseAssignProduct()
        {
            int UserId = AirBoxSession.User.UserId;
            int UserRoleId = AirBoxSession.User.UserRoleId;
            List<Product> lstProductDetails = _objAdminBusinessLayer.GetFranchiseAssignProduct(UserId, UserRoleId);
            return Json(lstProductDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllUnAssignProductToAdmin()
        {
            int UserId = AirBoxSession.User.UserId;
            int UserRoleId = AirBoxSession.User.UserRoleId;
            List<Product> lstProductDetails = _objAdminBusinessLayer.GetAllUnAssignProductToAdmin();
            return Json(lstProductDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignProduct(int productId, int pinId, string pinVal)
        {
            int productIdVal = _objAdminBusinessLayer.AssignProduct(productId, pinId, pinVal);
            return Json(productIdVal, JsonRequestBehavior.AllowGet);
        }
    }
}