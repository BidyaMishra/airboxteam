﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class AddVendorDetailsController : Controller
    {
        readonly VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
        //
        // GET: /AddVedorDetails/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddEditVendorDetails (Vendor vendorDetails)
        {
            int result = _objVendorBusinessLayer.AddEditVendorDetails(vendorDetails);
         return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorDetailsList()
        {
            List<Vendor> vendorDetails = _objVendorBusinessLayer.GetVendorDetailsList();
            return Json(vendorDetails, JsonRequestBehavior.AllowGet);
        }
	}
}