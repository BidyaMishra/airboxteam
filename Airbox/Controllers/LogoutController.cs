﻿using Airbox.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    public class LogoutController : Controller
    {
        //
        // GET: /Logout/
        public ActionResult Index()
        {

            AirBoxSession.User = null;
            return RedirectToAction("Index", "Login");
            
        }
	}
}