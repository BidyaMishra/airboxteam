﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class FranchiseDashBoardController : Controller
    {
        // GET: VendorDashBoard
        public ActionResult Index()
        {
            TempData["RoleId"] = AirBoxSession.User.UserRoleId;
            return View();
        }
        public ActionResult GetVendorDetailsList()
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            List<Vendor> vendorDetails = _objVendorBusinessLayer.GetVendorDetailsList();
            return Json(vendorDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditVendorDetails(Vendor vendorDetails)
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            int result = _objVendorBusinessLayer.AddEditVendorDetails(vendorDetails);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUserMaritalStatusList()
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            List<UserMaritalStatus> maritalStatusList = _objVendorBusinessLayer.GetUserMaritalStatusList();
            return Json(maritalStatusList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFranchiseProfileDetailsbyId()
        {
            int UserId = AirBoxSession.User.Id;
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            Vendor Userdetails = _objVendorBusinessLayer.GetFranchiseProfileDetailsbyId(UserId);
            return Json(Userdetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUserGenderList()
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            List<UserGender> Userdetails = _objVendorBusinessLayer.GetUserGenderList();
            return Json(Userdetails, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetFranchiseProfileDetailsbyFranchiesRelationId(int userRelationIdVal)
        {
            VendorBusinessLayer _objVendorBusinessLayer = new VendorBusinessLayer();
            Vendor vendorDetails = _objVendorBusinessLayer.GetFranchiseProfileDetailsbyId(userRelationIdVal);
            return Json(vendorDetails, JsonRequestBehavior.AllowGet);
        }
    }
}