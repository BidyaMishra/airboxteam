﻿using Airbox.Business;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    public class LoginController : Controller
    {
        readonly UserBusinessLayer _objUserBusinessLayer = new UserBusinessLayer();
        //
        // GET: /Login/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserLogin(string EmailPhone,string Password)
        {

          User user=_objUserBusinessLayer.UserLogin(EmailPhone, Password);
            //User user = new User();
             AirBoxSession.User = user;

           
            if (AirBoxSession.User.Id > 0)
            {

                return Json(user, JsonRequestBehavior.AllowGet);
           }
            else
            {

                return RedirectToAction("index", "LogIn");
            }
  

             return Json("", JsonRequestBehavior.AllowGet);
        }
	}
}