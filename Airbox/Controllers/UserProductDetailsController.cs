﻿using Airbox.Business;
using Airbox.SessionoutFilter;
using AirBox.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class UserProductDetailsController : Controller
    {
        readonly UserBusinessLayer _objUserBusinessLayer = new UserBusinessLayer();
        //
        // GET: /UserProductDetails/
        public ActionResult Index()
        {

            //@ViewBag.IsEditable  = AirBoxSession.User.UserRole;
            return View();
        }
        public ActionResult GetAllAssignedPeoductDetailsByVendorId()
        {
          int userId=AirBoxSession.User.Id;
            
            List<Product> productDetails = _objUserBusinessLayer.GetAllAssignedPeoductDetailsByVendorId(userId);
            return Json(productDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssignedProductDetailsByDeliveryBoyId()
        {
            int userId = AirBoxSession.User.Id;

            List<Product> productDetails = _objUserBusinessLayer.GetAssignedProductDetailsByDeliveryBoyId(userId);
            return Json(productDetails, JsonRequestBehavior.AllowGet);
        }
	}
}