﻿using Airbox.SessionoutFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airbox.Controllers
{
    [SessionExpireFilter]
    public class AdminDashBoardController : Controller
    {
        // GET: AdminDashBoard
        public ActionResult Index()
        {
            return View();
        }
    }
}